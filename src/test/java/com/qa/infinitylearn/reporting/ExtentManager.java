package com.qa.infinitylearn.reporting;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.infinitylearn.infinity.utils.DriverManager;
import com.relevantcodes.extentreports.ExtentReports;

//OB: ExtentReports extent instance created here. That instance can be reachable by getReporter() method.

public class ExtentManager extends DriverManager {

	//  private static String filename;

	  private static ExtentReports extent;
	  @BeforeSuite
	  public synchronized static ExtentReports getReporter(){
	      if(extent == null){
	          //Set HTML reporting file location
	          String workingDir = System.getProperty("user.dir");
	          DateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH_mm_ss");
	          DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
	          //File config=new File();   
	         //filename = workingDir+"\\ExtentReports\\InfinityLearn-"+df1.format(new Date())+"\\ExtentTest -"+df.format(new Date())+".html";
	        //  extent = new ExtentReports(filename , true);

	          extent = new ExtentReports(workingDir+"\\ExtentReports\\InfinityLearn-"+df1.format(new Date())+"\\ExtentTest -"+df.format(new Date())+".html", true);
	          extent.loadConfig(new File(workingDir+"\\config-report.xml"));
	          
	      }
	      return extent;}
}
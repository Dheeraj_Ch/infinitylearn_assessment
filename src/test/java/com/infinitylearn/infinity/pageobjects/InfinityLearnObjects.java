package com.infinitylearn.infinity.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.infinitylearn.infinity.utils.DriverManager;

public class InfinityLearnObjects {
	//		Main page
	@FindBy(xpath ="//a[@class='logo']/img") public WebElement InfinityLogo;
	
	@FindBy(xpath ="//descendant::a[contains(text(),'Join')][1]") public WebElement JoinFF1;
	@FindBy(xpath ="//a[text()='sign in']") public WebElement signfrmJ1;
	@FindBy(xpath ="//descendant::a[text()='JEE'][3]") public WebElement Jee3btn;
	@FindBy(xpath ="//descendant::a[text()='NEET'][2]") public WebElement JNEET2btn;
	
	
	/*//@FindBy(xpath ="//descendant::a[@id = 'navbarDropdown'][2]") public WebElement Coursesdropdwn;
	@FindBy(xpath ="//a[contains(text(),'Courses')]") public WebElement Coursesdropdwn;
	//@FindBy(xpath ="//descendant::a[text() = ' NEET'][2]") public WebElement NEETBtn;
	@FindBy(xpath ="//div/a[contains(text(),'NEET')]") public WebElement NEETBtn;
	//@FindBy(xpath ="//descendant::a[text() = ' JEE'][2]") public WebElement JEEBtn;
	@FindBy(xpath ="//div/a[contains(text(),'JEE')]") public WebElement JEEBtn;
	//@FindBy(xpath ="//descendant::img[@alt = 'Infinity Learn'][3]") public WebElement InfinityLogo;
	
	@FindBy(xpath ="//h1[text() = 'NEET 2021']") public WebElement NEETpage;
	@FindBy(xpath ="//descendant::h1[contains(text(), 'Ace')]") public WebElement JEEpage;
	//@FindBy(xpath = "//span[text()='fun']") public WebElement HomePageFun;-----Home page
	@FindBy(xpath = "//a[contains(text(),'Courses')]") public WebElement HomePageFun;	//--Homepage
	
	//@FindBy(xpath = "//descendant::a[text() = 'About Us'][2]") public WebElement AboutUs1;
	@FindBy(xpath = "//descendant::a[contains(text(),'About')][1]") public WebElement AboutUs1;
	@FindBy(xpath = "//h2[contains(text(), 'Our')]") public WebElement VerifyAboutPage;
	@FindBy(xpath = "//p[contains(text(),'NEET')]//following-sibling::button") public WebElement NeetExploreBtn;
	@FindBy(xpath = "//p[contains(text(),'Assess')]//following-sibling::button") public WebElement JeeExploreBtn;
	@FindBy(xpath = "//a[text() = 'NEET']") public WebElement NEETFooterBtn;
	@FindBy(xpath = "//a[text() = 'JEE']") public WebElement JEEFooterBtn;
	@FindBy(xpath = "//descendant::a[contains(text(),'About')][2]") public WebElement AboutUsFooterBtn;
	@FindBy(xpath = "//descendant::a[text() = 'Terms & Conditions'][1]") public WebElement TermsConditionsBtn;
	@FindBy(xpath = "//img[contains(@src, 'left-arrow')]") public WebElement BackTermsconditionsBtn;
	@FindBy(xpath = "//button[@class= 'closeButton']") public WebElement ClosePopUpB;*/
	//Social Links
	@FindBy(xpath = "//img[contains(@src,'facebook')]") public WebElement FaceBookLnk;
	@FindBy(xpath = "//img[contains(@src,'linkedin')]") public WebElement LinkedInLnk;
	@FindBy(xpath = "//img[contains(@src,'insta')]") public WebElement InstagramLnk;
	@FindBy(xpath = "//img[contains(@src,'youtube')]") public WebElement YoutubeLnk;
	@FindBy(xpath = "//img[contains(@src,'twitter')]") public WebElement TwitterLnk;
	//Login 
	@FindBy(xpath ="//descendant::a[contains(text(),'Sign in')][1]") public WebElement SigninBtn;
	@FindBy(xpath ="//descendant::input[contains(@placeholder,'Phone')][2]") public WebElement PhoneTxt;
	@FindBy(xpath ="//a[text() = 'LOGIN WITH PASSWORD']") public WebElement PasswordBtn;
	@FindBy(xpath ="//input[@type = 'password']") public WebElement PasswordTxb;
	@FindBy(xpath ="//button[contains(text(),'LOGIN')]") public WebElement LoginBtn;
	@FindBy(xpath = "//h4[text()='Infinity Learn Test Series (ILTS)']") public WebElement InfinityLTN;	
//	Navigate to Begin Test page
	@FindBy(xpath="//button[text()='NEET']") public WebElement NEETButton;
	@FindBy(xpath="//button[@class='btn-link active' and text()='NEET']") public WebElement NEETPageVerify;
	@FindBy(xpath="//button[@class='btn-link active' and text()='JEE Main']") public WebElement JeeActive;
	//descendant::div[@class = 'info-box h-100 ng-star-inserted'][3]/child::a[@type = 'button']
	//descendant::div[@class = 'info-box h-100 ng-star-inserted'][3]/child::a[@type = 'button']
		@FindBy(xpath = "//h4[text()='Mock Tests (MCTs)']//parent::div//child::a") public WebElement AttemptTstOpenLst;
		@FindBy(xpath = "//h4[text()='Mock Tests (MCTs)']//parent::div//child::a") public WebElement AttemptNew;

		@FindBy(xpath = "//button[contains(text(), 'ATTEMPT')]") public WebElement AttemptAnTst;
		@FindBy(xpath = "//a[contains(text(), 'Mock Tests')]") public WebElement MockTst;
		@FindBy(xpath = "//button[text()='BEGIN TEST']") public WebElement BegintstBtn;
	@FindBy(xpath = "//button[contains(text(),'BEGIN WALKTHROUGH')]") public WebElement BeginWalkThroughBtn;
	@FindBy(xpath = "//p[text() = 'Hi!']") public WebElement VerifyBeginWalk;			//	Verify Begin walk through is opened
//	Next button for begin walk through	
	@FindBy(xpath = "//button[text()= 'NEXT ']") public WebElement ClickNextBtn1;
	@FindBy(xpath = "//img[@src = 'assets/img/begin-walkthrough-step-2-img.png']") public WebElement Verfypage2;
	@FindBy(xpath = "//img[@src = 'assets/img/begin-walkthrough-step-3-img.png']") public WebElement Verfypage3;
	@FindBy(xpath = "//img[@src = 'assets/img/begin-walkthrough-step-4-img.png']") public WebElement Verfypage4;
	@FindBy(xpath = "//button[text() = 'Back to instructions ']") public WebElement BackToInstPageBtn;
	@FindBy(xpath = "//h3[text() = 'General Instructions']") public WebElement VerifyInstPage;
	@FindBy(xpath = "//div[@class='pad-input']") public WebElement Numpad;
	@FindBy(xpath = "//button[contains(text(),'1')]") public WebElement Num1;

	@FindBy(xpath = "//span[contains(text(), 'Q 1.')]") public WebElement FirstQstn;
	//For clicking on radio button no 2 of 1st question
	@FindBy(xpath = "//input[@id ='click2']") public WebElement ClickRadioBtn1;
	//For clicking on mark and review button
	@FindBy(xpath = "//button[text()='MARK FOR REVIEW ']") public WebElement MarkForReview;	
	//For verifying 2nd question
	@FindBy(xpath = "//span[contains(text(), 'Q 2.')]") public WebElement SecondQstn;
 	//For clicking on Clear and response
	@FindBy(xpath = "//button[text()='CLEAR RESPONSE']") public WebElement ClearResps;
	// For clicking on 5th question
	@FindBy(xpath = "//button[contains(text(),'5')]") public WebElement FifthBtn;
	@FindBy(xpath = "//button[contains(text(),'5')]") public WebElement FifthQbtn;

	@FindBy(xpath = "//span[text()=' Q 5. ']") public WebElement FifthQstn;			//For verifying 5nd question
	//For clicking on save
	@FindBy(xpath = "//span[contains(text(), 'AND NEXT')]//parent::button") public WebElement SaveBtn;
	@FindBy(xpath = "//img[contains(@src, 'next')]") public WebElement ForwardArrowBtn;
	@FindBy(xpath = "//img[contains(@src, 'prev')]") public WebElement BackWardArrowBtn;
	@FindBy(xpath = "//button[text()='CHEMISTRY']") public WebElement ChemistryBtn;
	@FindBy(xpath = "//button[text()='BIOLOGY']") public WebElement BiologyBtn;
	@FindBy(xpath = "//button[text()='SUBMIT']") public WebElement SubmitBtn;
	@FindBy(xpath = "//button[contains(text(),'Back')]") public WebElement BackToTestSubmtBtn;
	@FindBy(xpath = "//mat-dialog-container//button[text() = 'MEDIUM']") public WebElement MediumBtn;
	@FindBy(xpath = "//mat-dialog-container//button[text() = 'BAD']") public WebElement BadBtn;	
	@FindBy(xpath = "//mat-dialog-container//button[text()='Finish test']") public WebElement FinishTstBtn;
	@FindBy(xpath = "//button[text()='Submit']") public WebElement FinalSubmitBtn;
	//@FindBy(xpath = "//descendant::button[@class='dropdown-toggle'][2]") public WebElement ProfileIcon; Used before
	@FindBy(xpath = "//button[@class='dropdown-toggle']") public WebElement ProfileIcon;
	//	@FindBy(xpath = "//descendant::img[contains(@src, 'empty-profile.svg')][1]") public WebElement ProfileIcon;
//	@FindBy(xpath = "//descendant::img[contains(@src, 'logout-icon')][3]") public WebElement LogoutBtn;
//	@FindBy(xpath = "//descendant::button[@class='dropdown-toggle'][3]") public WebElement ProfileIconEdit;
	@FindBy(xpath = "//div[contains(@class,'profile')]/button") public WebElement ProfileIconEdit;
	@FindBy(xpath = "//descendant::img[contains(@src, 'logout')]//parent::button") public WebElement LogoutBtn;
	@FindBy(xpath = "//div/descendant::button[contains(text(),'View s')][1]") public WebElement PhyViewSoltnBtn;
	@FindBy(xpath = "//div/descendant::button[contains(text(),'View s')][2]") public WebElement ChemViewSoltnBtn;
	@FindBy(xpath = "//div/descendant::button[contains(text(),'View s')][3]") public WebElement BioViewSoltnBtn;
	@FindBy(xpath = "//div/descendant::button[contains(text(),'View s')][3]") public WebElement MathViewSoltnBtn;

	@FindBy(xpath = "//descendant::div[1]/descendant::button[contains(text(), 'View')][1]") public WebElement ViewSolnBtn;		//verify test completed with view solution btn
	@FindBy(xpath = "//img[contains(@src,'back-arrow')]//parent::div") public WebElement TestSolnsBckBtn;
	@FindBy(xpath = "//button[contains(@class,'btn-toggle m-hide')]") public WebElement ExtremeRightBtn;
	@FindBy(xpath = "//button[contains(@class,'btn-toggle-hide m-hide')]") public WebElement ExtremeLeftBtn;
	@FindBy(xpath = "//a[contains(text(),'Take')]") public WebElement TakeAnthrTst;		//View Solutions page Button
	@FindBy(xpath="//div//button[contains(text(),'Sec B')]") public WebElement SecBBtn;
	
	@FindBy(xpath="//div[1]/div/button[2][text()='Sec B']") public WebElement PhysiscsSectionViewSolutionsbutton;
	@FindBy(xpath="//div[2]/div/button[2][text()='Sec B']") public WebElement ChemistrySectionViewSolutionsbutton;
	@FindBy(xpath="//div[3]/div/button[2][text()='Sec B']") public WebElement BiologySectionViewSolutionsbutton;
	//	General instruction
	@FindBy(xpath = "//descendant::img[@src = 'assets/img/instruction-icon.svg'][1]") public WebElement GeneralInst;
	@FindBy(xpath = "//a[text() ='General Instructions']") public WebElement GeneralInstBtn;
	@FindBy(xpath = "//a[contains(text(),'GO BACK')]") public WebElement GoBackBtn;
	@FindBy(xpath = "//button[contains(text(),'GO BACK')]") public WebElement GoBackB;
	
	//	Question List
	@FindBy(xpath = "//descendant::img[@src = 'assets/img/question-list-icon.svg'][1]") public WebElement QuestionLst;
	@FindBy(xpath = "//a[text() = 'Question List']") public WebElement QuestionLstBtn;
	@FindBy(xpath = "//a[text() = 'BACK TO TEST']") public WebElement BackToTestBtn;
	// Previous papers
		@FindBy(xpath = "//h4[contains(text(),'Previous')]//parent::div//child::a") public WebElement PrePaperOpenList;
		@FindBy(xpath = "//a[contains(text() , 'Previous Year Papers')]") public WebElement VerifyPrePaprPage;
		@FindBy(xpath = "//button[text() = 'Attempt now']") public WebElement PrePaperAttemptNw;
		@FindBy(xpath = "//a[contains(text(),'Next')]") public WebElement NextPageBtn;

	// Report after Login page
		@FindBy(xpath = "//h4[contains(text() , 'My Report')]") public WebElement VerifyMyRpt;
		@FindBy(xpath = "//descendant::a[text() = 'VIEW DETAILED REPORT'][1]") public WebElement ViewDtldRptBtn;
		@FindBy(xpath = "//h4[text() = ' Overall Reports']") public WebElement VerifyWthOvrallrpts;
		@FindBy(xpath = "//button[text() = '15 Days']") public WebElement Days15Btn;
		@FindBy(xpath = "//button[text() = '1 Month']") public WebElement Month1Btn;
		@FindBy(xpath = "//button[text() = '6 Months']") public WebElement Month6Btn;
		@FindBy(xpath = "//button[text() = '1 Year']") public WebElement Year1Btn;
		@FindBy(xpath = "//button[text() = '1 Week']") public WebElement Week1Btn;
		@FindBy(xpath = "//div[contains(@class,'d-report')]//following-sibling::div") public WebElement DetailedRptDrp;
		//@FindBy(xpath = "//button[contains(text(),'View test')]") public WebElement ViewTestScheduleBtn;

	    @FindBy(xpath = "//span[text() = 'All Tests']") public WebElement AllTstDropDwn;
	    @FindBy(xpath = "//button//span[contains(text(),'ILTS')]") public WebElement InfiLrnTstDrpDwn;
	    //@FindBy(xpath = "//button//span[contains(text(),'Mock')]") public WebElement MockTstDrpDwn;
	    @FindBy(xpath = "//span[contains(text(),'MCT')]") public WebElement MockTstDrpDwn;
	    @FindBy(xpath = "//button//span[contains(text(),'Previous')]") public WebElement PrevYerPapDrpDwn;
	    //@FindBy(xpath = "//div//a[contains(text(),'Infinity')]") public WebElement InfiLrnTstSrsBtn;
	  //a[contains(text(),' Test Series (New Pattern)')]
	  //div//descendant::a[contains(text(),'Infinity')][2]
	    @FindBy(xpath = "//a[contains(text(),' Test Series (New Pattern)')]") public WebElement InfiLrnTstSrsBtn;
	    //@FindBy(xpath = "//div//a[contains(text(),'Mock')]") public WebElement MockTestBtn;
	    @FindBy(xpath = "//a[contains(text(),'Tests (New')]") public WebElement MockTestBtn;

	    @FindBy(xpath = "//div//a[contains(text(),'Previous')]") public WebElement PreYerPapBtn;
		@FindBy(xpath = "//div//a[contains(text(),'All')]") public WebElement AllTstBtn;
		@FindBy(xpath = "//button//span[contains(text(),'Over')]") public WebElement OverAllDrpDwn;
		@FindBy(xpath = "//button//span[contains(text(),'Time')]") public WebElement TimeTakenDrpDwn;
		@FindBy(xpath = "//div//a[contains(text(),'Over')]") public WebElement OverAllBtn;
		@FindBy(xpath = "//div//a[contains(text(),'Time')]") public WebElement TimeTakenBtn;
		@FindBy(xpath = "//div//descendant::button[contains(text(),'VIEW')][4]") public WebElement ChemistryViewRpt;
		@FindBy(xpath = "//div//descendant::button[contains(text(),'VIEW')][5]") public WebElement PhysicsViewRpt;
		@FindBy(xpath = "//div//descendant::button[contains(text(),'VIEW')][6]") public WebElement BiologyViewRpt;
//		InfinityTest Series
		@FindBy(xpath = "//div//child::p[contains(text(),'live')]//following-sibling::a") public WebElement TestSeriesOpenLstBtn;
		@FindBy(xpath = "//a[text()='Grid']") public WebElement GridView;
		@FindBy(xpath = "//a[contains(text(),'List')]") public WebElement ListView;
		@FindBy(xpath = "//button[contains(text(),'FILTER')]") public WebElement FilterBtn;
		@FindBy(xpath = "//div/a[text()='Attempted']") public WebElement AttemptedBtn;
		@FindBy(xpath = "//div/a[text()='Unattempted']") public WebElement UnattemptedBtn;
		@FindBy(xpath = "//div/a[text()='Clear']") public WebElement ClearBtn;
		@FindBy(xpath = "//a[contains(text(),'Test Series')]") public WebElement TestSeriesBackBtn;	
		// EDIT Profile
		//@FindBy(xpath = "//descendant::button[contains(text(),'Profile')][2]") public WebElement HomeProfileBtn; Used before
		@FindBy(xpath = "//button[contains(text(),'Profile')]") public WebElement HomeProfileBtn;

		@FindBy(xpath = "//h2[contains(text(), 'My Profile')]") public WebElement VerifyProfilePage;
		@FindBy(xpath = "//descendant::button[text() = 'EDIT PROFILE'][1]") public WebElement EditProfileBtn;
		@FindBy(xpath = "//button[@data-target = '#gradeModal']") public WebElement EditGradeBtn;
		@FindBy(xpath = "//button[@data-target = '#boardModal']") public WebElement EditBoardBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), '9th')]") public WebElement Grade9thBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), '13th')]") public WebElement Grade13thBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), '12th')]") public WebElement Grade12thBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), '10th')]") public WebElement Grade10thBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), '11th')]") public WebElement Grade11thBtn;
		@FindBy(xpath = "//mat-dialog-container//input[@id='currentPassword']") public WebElement CurrentPasswordTxb;

		@FindBy(xpath = "//mat-dialog-container//button[text() = 'SAVE DETAILS']") public WebElement SaveDetailsBtn;
		@FindBy(xpath = "//mat-dialog-container//descendant::div[contains(text(), 'Andhra')][1]") public WebElement APBoardBtn;
		@FindBy(xpath = "//mat-dialog-container//button[text() = 'SAVE PREFERENCE']") public WebElement SavePreferencesBtn;
		@FindBy(xpath = "//img[contains(@src, 'target-exam')]//following-sibling::button") public WebElement EDITTargetExamBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), 'JEE Main')]") public WebElement TargetJeeBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), 'NEET')]//following-sibling::div") public WebElement TargetNeetBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), 'NEET')]") public WebElement TargetJeeAdvancedBtn;
		@FindBy(xpath = "//img[contains(@src,'green-tick')]//parent::div//child::div[contains(text(),'NEET')]") public WebElement NeetTargetSelected;

		@FindBy(xpath = "//img[contains(@src,'green-tick')]//parent::div//child::div[contains(text(),'JEE Main')]") public WebElement JeeMainTargetSelected;
		@FindBy(xpath = "//img[contains(@src,'green-tick')]//parent::div//child::div[contains(text(),'JEE Advanced')]") public WebElement JeeAdvTargetSelected;
		@FindBy(xpath = "//button[contains(text(),'Password')]") public WebElement ChangePasswordBtn;
		@FindBy(xpath = "//mat-dialog-container//h4[contains(text(),'password')]") public WebElement VerifyChangePassword;
		@FindBy(xpath = "//mat-dialog-container//input[@formcontrolname='newPassword']") public WebElement NewPasswordTxb;
		@FindBy(xpath = "//mat-dialog-container//input[@formcontrolname='confirmPassword']") public WebElement ConfirmPasswordTxb;
		@FindBy(xpath = "//mat-dialog-container//button[contains(text(),'CONFIRM')]") public WebElement ConfirmPasswordBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(),'updated')]") public WebElement VerifyPasswordUpdated;
		@FindBy(xpath = "//mat-dialog-container//button[text()='CLOSE']") public WebElement CloseBtn;
		@FindBy(xpath = "//button[text() = 'SAVE DETAILS']") public WebElement FinalSaveDetailsBtn;
		@FindBy(xpath = "//div/h4[text()='Profile Updated']") public WebElement VerifyProfileUpdted;
		//Login using OTP
		@FindBy(xpath = "//mat-dialog-container//descendant::input[contains(@class,'otp')][1]") public WebElement OtpTxtB;
		@FindBy(xpath = "//h3[contains(text(),'Hello')]") public WebElement VerifyLogin;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), 'PCMB')]") public WebElement PcmbStream;
		@FindBy(xpath = "//input[@placeholder='Password']") public WebElement SetPasswordTxtB;
		@FindBy(xpath = "//descendant::input[@placeholder='Password'][2]") public WebElement SetConfirmPassTxtB;
		/*@FindBy(xpath = "//mat-dialog-container//input[@placeholder='First Name']") public WebElement FirstNameTxtB;
		@FindBy(xpath = "//mat-dialog-container//input[@placeholder='Last Name']") public WebElement LastNameTxtB;
		@FindBy(xpath = "//mat-dialog-container//label[contains(text(),'Whatsapp')]//preceding-sibling::input") public WebElement WhatsappChkB;
		@FindBy(xpath = "//mat-dialog-container//button[contains(text(),'REGISTER')]") public WebElement RegisterBtn;
		@FindBy(xpath = "//mat-dialog-container//a[contains(text(),'EDIT')]") public WebElement EditBtn;
		@FindBy(xpath = "//mat-dialog-container//descendant::input[@id='phone'][4]") public WebElement EditPhone;
		@FindBy(xpath = "//mat-dialog-container//button[text()='NEXT']") public WebElement EditNxtBtn;
		
		@FindBy(xpath = "//mat-dialog-container//button[contains(text(),'VERIFY')]") public WebElement VerifyOtpBtn;
		
		@FindBy(xpath = "//mat-dialog-container//button[contains(text(),'RESEND')]") public WebElement ResendOtpBtn;
		@FindBy(xpath = "//mat-dialog-container//h4[contains(text(),'Account Created')]") public WebElement VerifyAccountCrtd;
		@FindBy(xpath = "//mat-dialog-container//a[contains(text(),'START')]") public WebElement StartOnBrdBtn;
		@FindBy(xpath = "//mat-dialog-container//input[@placeholder='Email']") public WebElement EmailTxtB;
		
		
		@FindBy(xpath = "//mat-dialog-container//button[contains(text(),'NEXT')]") public WebElement SetUpAccNextBtn;
		
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), 'GET')]") public WebElement GetStartedBtn;*/
		//Forgot password
		@FindBy(xpath = "//button[text()= 'Send OTP']") public WebElement SendOTPBtn;	
		@FindBy(xpath = "//a[contains(text(),'FORGOT')]") public WebElement ForgotPassBtn;
		@FindBy(xpath = "//descendant::button[contains(text(),'Get OTP')][2]") public WebElement GetOTPBtn;
		@FindBy(xpath = "//button[contains(text(),'RESET')]") public WebElement ResetPassBtn;
		@FindBy(xpath = "//mat-dialog-container//h4[contains(text(),'reset')]") public WebElement VerifyResetPass;	
		//Test Cases Page Login
		@FindBy(xpath="//descendant::button[contains(text(),'START FOR')]") public WebElement StartForFreeLiveClassesBtn;
		@FindBy(xpath="//descendant::button[contains(text(),'START FOR')][2]") public WebElement StartForTestSeriesBtn;
		@FindBy(xpath ="//button[contains(text(),'LOGIN')]") public WebElement LoginFrstBtn;
		@FindBy(xpath ="//mat-dialog-container//button[contains(text(),'X')]") public WebElement CancelXBtn;
		@FindBy(xpath ="//descendant::input[@id='phone'][4]") public WebElement FirstPhoneTxt;
		@FindBy(xpath ="//descendant::input[@id='phone'][3]") public WebElement PhoneSndTxt;
		@FindBy(xpath ="//descendant::button[contains(text(),'START LEARNING')][3]") public WebElement StrtLrngBtn;
		@FindBy(xpath ="//a[contains(text(),'schedule')]") public WebElement ScheduleBtn;		//Neet Page
		@FindBy(xpath ="//descendant::input[@id='phone'][2]") public WebElement NeetPhoneTxt;
		@FindBy(xpath ="//descendant::button[contains(text(),'START LEARNING')][2]") public WebElement NeetStrtBtn;
		@FindBy(xpath ="//button[(text()='REGISTER')]") public WebElement NeetRegisterBtn;				
		@FindBy(xpath = "//mat-dialog-container//input[@placeholder='First Name']") public WebElement FrstNameTxtB;
		@FindBy(xpath ="//mat-dialog-container//div//input[contains(@placeholder,'Mobile')]") public WebElement RegisterMobile;		
		@FindBy(xpath ="//mat-dialog-container//div//button[contains(text(),'10th')]") public WebElement TenthGrdBtn;
		@FindBy(xpath ="//mat-dialog-container//div//button[contains(text(),'11th')]") public WebElement EleventhGrdBtn;
		@FindBy(xpath ="//mat-dialog-container//div//button[contains(text(),'12th')]") public WebElement TwelthGrdBtn;
		@FindBy(xpath ="//mat-dialog-container//button[@class='closeButton']") public WebElement RegisterCloseBtn;
		@FindBy(xpath ="//descendant::div[contains(@class,'profile')][3]//child::button//img") public WebElement ProfileBtn;
		@FindBy(xpath ="//descendant::button[contains(text(),'Logout')][3]") public WebElement Logout;
		//Test Cases EDIT Profile Page 
		@FindBy(xpath ="//mat-dialog-container//button[text()='Cancel']") public WebElement CancelgradeBtn;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), 'PCM')]") public WebElement PcmStream;
		@FindBy(xpath = "//mat-dialog-container//div[contains(text(), 'PCB')]") public WebElement PcbStream;
		@FindBy(xpath = "//mat-dialog-container//div[@class='boards-box assessment_pt-20']//child::div[@class='board-box ng-star-inserted']") public WebElement Board;
		//Mock Test Page
		@FindBy(xpath = "//a[contains(text(),'Mock')]") public WebElement MockBckBtn;
		@FindBy(xpath = "//button[contains(text(),'GO BACK')]") public WebElement RptGoBckBtn;
		@FindBy(xpath = "//a//span[text()='1']") public WebElement Page1QstLst;		//question list page 1
		@FindBy(xpath = "//a//span[text()='2']") public WebElement Page2QstLst;		//question list page 2
		@FindBy(xpath = "//a//span[text()='3']") public WebElement Page3QstLst;		//question list page 3
		@FindBy(xpath = "//a//span[text()='4']") public WebElement Page4QstLst;		//question list page 4
		@FindBy(xpath = "//descendant::a[contains(text(),'Go To Question')][1]") public WebElement GoToQstn;		//Go to question 1
		@FindBy(xpath = "//input[contains(@value,'not_answer')]") public WebElement NtAnsChkBx;		//Not answered check box
		@FindBy(xpath = "//input[@value='answered']") public WebElement AnsChkBx;		// answered check box
		@FindBy(xpath = "//input[contains(@value,'not_visit')]") public WebElement NtVisitCkBx;		// Not visited check box
		@FindBy(xpath = "//input[contains(@value,'not_ans_review')]") public WebElement NansRw;		// Not answered and Marked for review check box
		@FindBy(xpath = "//input[contains(@value,'_ans_review')]") public WebElement AnsRw;		// Answered and Marked for review check box

		//JEEMain
		@FindBy(xpath = "//button[text()='JEE Main']") public WebElement JeeMainMdle;		
		//@FindBy(xpath = "//button[text()='JEE Main']") public WebElement JeeMainMdle;		 
		@FindBy(xpath="//button[contains(text(),'Main')]") public WebElement JeeButton;
		@FindBy(xpath="//button[contains(text(),'Advanced')]") public WebElement JeeAdvButton;
		@FindBy(xpath="//descendant::div[@class = 'info-box h-100 ng-star-inserted'][3]/child::a[@type = 'button']") public WebElement JeeAttemptTest;
		@FindBy(xpath="//button[@class='btn-link active' and text()='JEE Main']") public WebElement JEEMainPageVerify;
		@FindBy(xpath="//button[@class='btn-link active' and text()='JEE Advanced']") public WebElement JEEAdvPageVerify;
		@FindBy(xpath="//button[text()='MATHEMATICS']") public WebElement MathsBtn;

		// Live Classes
		@FindBy(xpath = "//img[contains(@alt,'Live')]//parent::button") public WebElement LiveClsMdleBtn;
		//
		@FindBy(xpath = "//input[contains(@PLACEHOLDER,'ADMISSION')]") public WebElement AdmissionNoTxb;
		@FindBy(xpath = "//mat-dialog-container//input[contains(@placeholder,'Password')]") public WebElement AdPaswd;
		@FindBy(xpath = "//h4[contains(text(),'Final Infinity National')]") public WebElement VerifyFinal;
		@FindBy(xpath = "//h4[contains(text(),'Final Infinity National')]//parent::div//child::a") public WebElement FinalOpenList;
		@FindBy(xpath = "//a[contains(text(),'Final Infinity')]") public WebElement ExamPage;
		//Neet Report
		@FindBy(xpath = "//button[text()='View Test report']") public WebElement ViewRptBtn;
		// Scholarship
		@FindBy(xpath = "//a[contains(text(),'Scholarship')]") public WebElement schlshpdrpdwn;
		@FindBy(xpath = "//a[contains(text(),'ILITE')]") public WebElement Ilitebtn;
		@FindBy(xpath = "//div[@class='iliteneet-box']//child::a//button[text()='SYLLABUS']") public WebElement NSyllabusBtn;
		@FindBy(xpath = "//div[@class='ilitejee-box']//child::a//button[text()='SYLLABUS']") public WebElement JMSyllabusBtn;
		@FindBy(xpath = "//h3[contains(text(),'Advanced')]//parent::div//child::a//button[text()='SYLLABUS']") public WebElement JASyllabusBtn;
		@FindBy(xpath = "//h4//Strong[text()='Syllabus']") public WebElement VrfySyllabus;	
		@FindBy(xpath = "//li//a[text()='JEE Main']") public WebElement JMBtn;
		@FindBy(xpath = "//li//a[text()='JEE Advanced']") public WebElement JABtn;
		@FindBy(xpath = "//descendant::a[text()='NEET'][1]") public WebElement NBtn1;
		@FindBy(xpath = "//a[contains(text(),'Terms')]") public WebElement TermsBtn;
		@FindBy(xpath = "//h3[contains(text(),'Terms')]") public WebElement VrfyTerms;
		@FindBy(xpath = "//descendant::a[text()='NEET'][2]") public WebElement NBtn2;
		@FindBy(xpath = "//a[text()='JEE']") public WebElement JBtn;
		@FindBy(xpath = "//img[contains(@src,'whatsapp')]") public WebElement WhatsappBtn;
		//Admission number
		@FindBy(xpath = "//span[text()='Login here']") public WebElement SCClickBtn;
		@FindBy(xpath = "//descendant::button[text()='START LEARNING'][1]") public WebElement StartBtn;

		
		
		
		public InfinityLearnObjects(){
		WebDriver driver = DriverManager.WEB_DRIVER_THREAD.get();				
		PageFactory.initElements(driver, this);	
	}

}

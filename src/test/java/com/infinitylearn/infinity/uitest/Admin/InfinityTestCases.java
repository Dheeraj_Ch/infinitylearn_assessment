package com.infinitylearn.infinity.uitest.Admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.infinity.config.Config;
import com.infinitylearn.infinity.libraries.InfinityLearnLibrary;
import com.infinitylearn.infinity.utils.DriverManager;
import com.qa.infinitylearn.reporting.ExtentManager;
import com.qa.infinitylearn.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import io.restassured.response.Response;
import com.infinitylearn.infinity.utils.ExcelUtil;


public class InfinityTestCases extends DriverManager {
	protected static String className;
	protected static HashMap<Integer, HashMap<String, String>> testData;
	protected InfinityLearnLibrary InfinityLearnLibrary;
	private ThreadLocal<String> testName = new ThreadLocal<>();
	static ExtentTest test;
	static ExtentReports report;	
	public static Response Response;
	
	
	@BeforeMethod
	public void methodsetup() throws Exception{
	InfinityLearnLibrary = new InfinityLearnLibrary();
	//InfinityLearnLibrary.WebBrowser(Config.APP_URL);
	}
	@DataProvider(name="getData", parallel = false)
	public Iterator<Object[]> getTestData() throws IOException {
		className = this.getClass().getSimpleName();
		testData = ExcelUtil.getTestData(className);
		
		ArrayList <Object[]> dataProvider = new ArrayList<Object[]>();
		for (Integer currentKey : testData.keySet()) {
			dataProvider.add(new Object[] { testData.get(currentKey) } );
		}
		return dataProvider.iterator();	
	}
	
	/*public void LoginPageTC(HashMap<String, String> testdata) throws Exception			//@Test(enabled=false)
	{			
		String sResult = null;
		ExtentTestManager.startTest("Full LoginPage Verification","");		
		InfinityLearnLibrary.WebBrowser(testdata);
		
	} 	*/
	
	@Test(priority=2,dataProvider = "getData")
	public void EditProfileTC(HashMap<String, String> testdata) throws Exception			//@Test(enabled=false)
	{	
		String sResult = null;
		ExtentTestManager.startTest("Full Edit Profile Verification","");
		InfinityLearnLibrary.WebBrowser(testdata);

		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.EditProfileTC();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
	}
	@Test(priority=3,dataProvider = "getData")
	public void MockTstTC(HashMap<String, String> testdata) throws Exception			//@Test(enabled=false)
	{	
		String sResult = null;
		ExtentTestManager.startTest("Jee Main Reports Verification","");
		InfinityLearnLibrary.WebBrowser(testdata);

		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Edit profile for selecting required Grade
		sResult = InfinityLearnLibrary.EditProfile(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.NavigatetoJEEMains();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.MockRptTC();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.ReportPage();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };	
	}
	
	@Test(priority=3,dataProvider = "getData")
	public void AdmissionNoLogin(HashMap<String, String> testdata) throws Exception
	{
		String sResult = null;
		ExtentTestManager.startTest("Login with Admission Verification","");
		InfinityLearnLibrary.WebBrowser(testdata);
		
		sResult = InfinityLearnLibrary.LoginWithAdmission(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };		
		
		sResult = InfinityLearnLibrary.Logout();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };	
	}
	@Test(priority=5, dataProvider = "getData")
	public void ScholarShip(HashMap<String, String> testdata) throws Exception {	
		String sResult = null;
		ExtentTestManager.startTest("Scholarship Test page verification", "");
		InfinityLearnLibrary.WebBrowser(testdata);
		
		sResult = InfinityLearnLibrary.ScholarshipTest(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };	
	}

	private void TearDown() throws Exception {
		throw new Exception("Test can't continue,  fail here!");
	}
	
	@AfterMethod
	public static void EndMethod() {
		ExtentManager.getReporter().flush();
		ExtentTestManager.endTest();
	}
	@AfterSuite
	public static void endSuite() {
		ExtentManager.getReporter().flush();
		ExtentTestManager.endTest();
	}
}

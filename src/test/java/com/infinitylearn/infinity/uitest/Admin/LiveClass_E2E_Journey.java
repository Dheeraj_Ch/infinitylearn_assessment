package com.infinitylearn.infinity.uitest.Admin;

import com.infinitylearn.infinity.utils.DriverManager;
import com.qa.infinitylearn.reporting.ExtentManager;
import com.qa.infinitylearn.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.response.Response;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.infinitylearn.infinity.utils.ExcelUtil;
import com.infinitylearn.infinity.utils.WebInteractUtil;
import com.infinity.config.Config;

import com.infinitylearn.infinity.libraries.InfinityLearnLibrary;
import com.infinitylearn.infinity.libraries.InfinityLearnAPILibrary;
import com.infinitylearn.infinity.utils.LoggerUtil;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LiveClass_E2E_Journey extends DriverManager
{
	protected static String className;
	protected static HashMap<Integer, HashMap<String, String>> testData;
	protected InfinityLearnLibrary InfinityLearnLibrary;
	
	private ThreadLocal<String> testName = new ThreadLocal<>();
	static ExtentTest test;
	static ExtentReports report;
	public static Response Response;

	@BeforeMethod
	public void methodsetup() throws Exception{
	InfinityLearnLibrary = new InfinityLearnLibrary();
	}
	
	@DataProvider(name = "getData", parallel = false)
	public Iterator<Object[]> getTestData() throws IOException {
		className = this.getClass().getSimpleName();
		testData = ExcelUtil.getTestData(className);
		ArrayList <Object[]> dataProvider = new ArrayList<Object[]>();
		for (Integer currentKey : testData.keySet()) {
			dataProvider.add(new Object[] { testData.get(currentKey)});		
		}
		return dataProvider.iterator();	
	}
	
	@Test(dataProvider = "getData",enabled = false)
	public void TS01_LiveClass_E2E_Journey(HashMap<String, String> testdata) throws Exception			//@Test(enabled=false)
	{			
		String sResult = null;
		int Statuscode;
		//Reading the Varaibles from the datasheet
		String apiURL = testdata.get("apiURL");
		String APIEndPoint = testdata.get("APIEndPoint");
		String MethodType = testdata.get("MethodType");
		String InputDataType = testdata.get("InputDataType");
		String InputData = testdata.get("InputData");
		String CreationType = testdata.get("CreationType");
		ExtentTestManager.startTest("Live Classes-"+CreationType+" creation", "");
		Map<String, String> authheaders = new HashMap<String, String>();
		authheaders.put("Content-Type", "application/json; charset=utf-8");
		System.out.println(CreationType);
		Response = InfinityLearnAPILibrary.Call_API(authheaders, apiURL, APIEndPoint, MethodType, InputDataType, InputData);	
		Statuscode=Response.getStatusCode();
	if(Statuscode==201) {
		switch(CreationType) {
		case "Recomendedclass":
			LoggerUtil.printExtentLog("Pass", "Response Body is"+Response.asString());		
			InfinityLearnLibrary.WebBrowser(testdata);
			
			sResult = InfinityLearnLibrary.LoginInfinity(testdata);
			if (sResult.equalsIgnoreCase("False")) {TearDown(); };
			
			sResult = InfinityLearnLibrary.LiveClassesPage();
			if (sResult.equalsIgnoreCase("False")) {TearDown();};		
			break;
		
		case "FreeMasterClass":
		InfinityLearnLibrary.WebBrowser(testdata);
		
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.LiveClassesPage();
		if (sResult.equalsIgnoreCase("False")) {TearDown();};	
		break;

		case "Course":
		InfinityLearnLibrary.WebBrowser(testdata);
		
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.LiveClassesPage();
		if (sResult.equalsIgnoreCase("False")) {TearDown();};		
		break;
		}
	Response = InfinityLearnAPILibrary.Call_API(authheaders, apiURL, APIEndPoint, "delete", InputDataType, InputData);	
	Statuscode=Response.getStatusCode();
	if(Statuscode==201) {
		LoggerUtil.printExtentLog("Pass", "Response Body is"+Response.asString());				
		LoggerUtil.printExtentLog("Pass", "Deleted"+ CreationType+" successfully");
	}else {
		LoggerUtil.printExtentLog("Fail", "Response Body is"+Response.asString());				
		LoggerUtil.printExtentLog("Fail", "Unable to delete"+ CreationType);
		TearDown();
	}
		}else {
		LoggerUtil.printExtentLog("Fail", "Response Body is"+Response.asString());				
		LoggerUtil.printExtentLog("Fail", "Unable to create"+ CreationType+", Please verify");
		TearDown();
		}
	}

	private void TearDown() throws Exception {
		throw new Exception("Test can't continue,  fail here!");
		}
	@AfterMethod
	public static void EndMethod() {
		ExtentManager.getReporter().flush();
		ExtentTestManager.endTest();
	}
	@AfterSuite
	public static void endSuite() {
		ExtentManager.getReporter().flush();
		ExtentTestManager.endTest();
	}
}

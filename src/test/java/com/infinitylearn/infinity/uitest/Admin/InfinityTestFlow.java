package com.infinitylearn.infinity.uitest.Admin;

import com.infinitylearn.infinity.utils.DriverManager;
import com.qa.infinitylearn.reporting.ExtentManager;
import com.qa.infinitylearn.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.qa.infinitylearn.reporting.*;


import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import com.infinitylearn.infinity.utils.ExcelUtil;
import com.infinitylearn.infinity.utils.WebInteractUtil;
import com.infinity.config.Config;
import com.infinitylearn.infinity.libraries.InfinityLearnLibrary;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class InfinityTestFlow extends DriverManager
{
	protected static String className;
	protected static HashMap<Integer, HashMap<String, String>> testData;
	protected InfinityLearnLibrary InfinityLearnLibrary;
	private ThreadLocal<String> testName = new ThreadLocal<>();
	static ExtentTest test;
	static ExtentReports report;

	
	@BeforeMethod
	public void methodsetup() throws Exception{
	InfinityLearnLibrary = new InfinityLearnLibrary();
	}
	@DataProvider(name = "getData", parallel = false)
	public Iterator<Object[]> getTestData() throws IOException {
		className = this.getClass().getSimpleName();
		testData = ExcelUtil.getTestData(className);	
		ArrayList <Object[]> dataProvider = new ArrayList<Object[]>();
		for (Integer currentKey : testData.keySet()) {
			dataProvider.add(new Object[] { testData.get(currentKey)});		
		}
		return dataProvider.iterator();	}
	@Test(priority=2,dataProvider = "getData")
	public void LoginPage(HashMap<String, String> testdata) throws Exception			//@Test(enabled=false)
	{			
		String sResult = null;
		ExtentTestManager.startTest("LoginPage Verification","");		
		InfinityLearnLibrary.WebBrowser(testdata);

		sResult = InfinityLearnLibrary.HomePage(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };		 
	} 		
	
	@Test(priority=3,dataProvider = "getData")		//Neet Mock
	public void NeetInfinityTest(HashMap<String, String> testdata) throws Exception
	{			
		String sResult = null;
		ExtentTestManager.startTest("Neet Mock Assessment Verification", "");	
		InfinityLearnLibrary.WebBrowser(testdata);

		// Login to InfinityLearn
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Selecting Grade
		sResult = InfinityLearnLibrary.SetGradeTargtExam(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.NavigateNeet();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.NavigateToAsessment();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//BeginWalkThrough page is verified
		sResult = InfinityLearnLibrary.BeginWalkThrough();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//Test Completion
		sResult = InfinityLearnLibrary.CompleteTest();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		// Logout
		sResult = InfinityLearnLibrary.Logout();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };		
	} 	
	
	@Test(priority = 4,dataProvider = "getData")		//Neet PYP's
	public void NeetPreviousPaperTest(HashMap<String, String> testdata) throws Exception {	
		String sResult = null;
		ExtentTestManager.startTest("Neet Previous Papers Assessment Verification", "");
		InfinityLearnLibrary.WebBrowser(testdata);

		// Login to InfinityLearn
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Selecting Grade
		sResult = InfinityLearnLibrary.SetGradeTargtExam(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };	
		
		sResult = InfinityLearnLibrary.NavigateNeet();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		//	Navigate to previous papers Test page
		sResult = InfinityLearnLibrary.NavigatePrevTstPage();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//BeginWalkThrough page is verified
		sResult = InfinityLearnLibrary.BeginWalkThrough();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//Test Completion
		sResult = InfinityLearnLibrary.CompleteTest();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Logout
		sResult = InfinityLearnLibrary.Logout();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };	
	} 
	
	@Test(priority=8,dataProvider = "getData")			//	Jee Main Mock
	public void JEEMainMock(HashMap<String, String> testdata) throws Exception
	{			
		String sResult = null;
		ExtentTestManager.startTest("JEE Main Mock Test Verification", "");	
		InfinityLearnLibrary.WebBrowser(testdata);
	
		// Login to InfinityLearn
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Selecting Grade
		sResult = InfinityLearnLibrary.SetGradeTargtExam(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.NavigatetoJEEMains();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.NavigateToAsessment();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//BeginWalkThrough page is verified
		sResult = InfinityLearnLibrary.BeginWalkThrough();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//Test Completion
		sResult = InfinityLearnLibrary.CompleteTest();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		// Logout
		sResult = InfinityLearnLibrary.Logout();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };		
	} 
	
	@Test(priority = 10,dataProvider = "getData")			//	Jee Main PYP's
	public void JeeMainPreviousPaperTest(HashMap<String, String> testdata) throws Exception {	
		String sResult = null;
		ExtentTestManager.startTest("JEE Main Previous Papers Assessment Verification", "");
		InfinityLearnLibrary.WebBrowser(testdata);

		// Login to InfinityLearn
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Selecting Grade
		sResult = InfinityLearnLibrary.SetGradeTargtExam(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.NavigatetoJEEMains();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//	Navigate to previous papers Test page
		sResult = InfinityLearnLibrary.NavigatePrevTstPage();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//BeginWalkThrough page is verified
		sResult = InfinityLearnLibrary.BeginWalkThrough();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//Test Completion
		sResult = InfinityLearnLibrary.CompleteTest();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Logout
		sResult = InfinityLearnLibrary.Logout();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };	
	} 
	
	@Test(priority = 11,dataProvider = "getData")			//	Jee Advanced Mock
	public void JEEAdvancedMock(HashMap<String, String> testdata) throws Exception {	
		String sResult = null;
		ExtentTestManager.startTest("JEE Main Previous Papers Assessment Verification", "");
		InfinityLearnLibrary.WebBrowser(testdata);
		// Login to InfinityLearn
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Selecting Grade
		sResult = InfinityLearnLibrary.SetGradeTargtExam(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.NavigateJeeAdvanced(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.NavigateToAsessment();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//BeginWalkThrough page is verified
		sResult = InfinityLearnLibrary.BeginWalkThrough();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//Test Completion
		sResult = InfinityLearnLibrary.CompleteTest();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		// Logout
		sResult = InfinityLearnLibrary.Logout();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };		
	}
	
	@Test(priority = 11,dataProvider = "getData")			//	Jee Advanced PYP's
	public void JEEAdvancedPYP(HashMap<String, String> testdata) throws Exception {	
		String sResult = null;
		ExtentTestManager.startTest("JEE Main Previous Papers Assessment Verification", "");
		InfinityLearnLibrary.WebBrowser(testdata);
		// Login to InfinityLearn
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Selecting Grade
		sResult = InfinityLearnLibrary.SetGradeTargtExam(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.NavigateJeeAdvanced(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//Navigate to previous papers Test page
		sResult = InfinityLearnLibrary.NavigatePrevTstPage();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//BeginWalkThrough page is verified
		sResult = InfinityLearnLibrary.BeginWalkThrough();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		//Test Completion
		sResult = InfinityLearnLibrary.CompleteTest();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		// Logout
		sResult = InfinityLearnLibrary.Logout();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };	
		
	}
	
	//	Reports-- Need to check and update
	@Test(enabled = false,dataProvider = "getData")
	public void VerifyRepotsPage(HashMap<String, String> testdata) throws Exception {
		
		String sResult = null;
		ExtentTestManager.startTest("JeeMain Reports Page, Filters, Grid and List View Verification", "");	
		InfinityLearnLibrary.WebBrowser(testdata);
		
		// Login to InfinityLearn
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		// Filter and View options
		sResult = InfinityLearnLibrary.NavigatetoJEEMains();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		sResult = InfinityLearnLibrary.InfinityTestSeries();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Reports Page
		sResult = InfinityLearnLibrary.ReportPage();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };	
	}
	//Edit Profile
	@Test(enabled = false,dataProvider = "getData")
	public void VerifyEditProfile(HashMap<String, String> testdata) throws Exception {
		String sResult = null;
		ExtentTestManager.startTest("Edit Profile Page Verification", "");	
		InfinityLearnLibrary.WebBrowser(testdata);

		// Login to InfinityLearn
		sResult = InfinityLearnLibrary.LoginInfinity(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
		// Verify Edit Profile
		sResult = InfinityLearnLibrary.EditProfile(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };			
	}	
	@Test(priority = 6,dataProvider = "getData")	//priority = 1
	public void LoginOTP(HashMap<String, String> testdata) throws Exception
	{			
		String sResult = null;
		ExtentTestManager.startTest("OTP Login Verification", "");	
		InfinityLearnLibrary.WebBrowser(testdata);

		// Login to InfinityLearn using OTP
		sResult = InfinityLearnLibrary.OTPSignIn(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		// Logout
		sResult = InfinityLearnLibrary.Logout();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };	
	}
	@Test(priority = 7,dataProvider = "getData")		//priority = 1
	public void ResetPassword(HashMap<String, String> testdata) throws Exception
	{			
		String sResult = null;
		ExtentTestManager.startTest("Forgot Password Verification", "");	
		InfinityLearnLibrary.WebBrowser(testdata);
	
		// Login to InfinityLearn using OTP
		sResult = InfinityLearnLibrary.ForgotPassword(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
	}/*
	@Test(enabled = false,dataProvider = "getData")
	public void RegisterUser(HashMap<String, String> testdata) throws Exception
	{			
		String sResult = null;
		ExtentTestManager.startTest("New User Registration", "");	
		InfinityLearnLibrary.WebBrowser(testdata);
	
		// Login to InfinityLearn using OTP
		sResult = InfinityLearnLibrary.OTPNewUser(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
	}*/


	
	private void TearDown() throws Exception {
		throw new Exception("Test can't continue,  fail here!");
	}
	@AfterMethod
	public static void EndMethod() {
		ExtentManager.getReporter().flush();
		ExtentTestManager.endTest();
	}
	@AfterSuite
	public static void endSuite() {
		ExtentManager.getReporter().flush();
		ExtentTestManager.endTest();
		//EmailNotification er =new EmailNotification();
		//er.Email();
	}
}

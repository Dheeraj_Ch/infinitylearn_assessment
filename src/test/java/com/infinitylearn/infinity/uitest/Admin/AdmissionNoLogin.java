package com.infinitylearn.infinity.uitest.Admin;

import com.infinitylearn.infinity.utils.DriverManager;
import com.qa.infinitylearn.reporting.ExtentManager;
import com.qa.infinitylearn.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.response.Response;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.infinitylearn.infinity.utils.ExcelUtil;
import com.infinitylearn.infinity.utils.WebInteractUtil;
import com.infinity.config.Config;

import com.infinitylearn.infinity.libraries.InfinityLearnLibrary;
import com.infinitylearn.infinity.libraries.InfinityLearnAPILibrary;
import com.infinitylearn.infinity.utils.LoggerUtil;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AdmissionNoLogin extends DriverManager {
		protected static String className;
		protected static HashMap<Integer, HashMap<String, String>> testData;
		protected InfinityLearnLibrary InfinityLearnLibrary;
		
		private ThreadLocal<String> testName = new ThreadLocal<>();
		static ExtentTest test;
		static ExtentReports report;
		public static Response Response;

		@BeforeMethod
		public void methodsetup() throws Exception{
		InfinityLearnLibrary = new InfinityLearnLibrary();
		}
		
		@DataProvider(name = "getData", parallel = false)
		public Iterator<Object[]> getTestData() throws IOException {
			className = this.getClass().getSimpleName();
			testData = ExcelUtil.getTestData(className);
			ArrayList <Object[]> dataProvider = new ArrayList<Object[]>();
			for (Integer currentKey : testData.keySet()) {
				dataProvider.add(new Object[] { testData.get(currentKey)});		
			}
			return dataProvider.iterator();	
		}
		
		@Test(dataProvider = "getData")
		public void LoginAdmissionNo(HashMap<String, String> testdata) throws Exception			//@Test(enabled=false)
		{
			String sResult = null;
			
			ExtentTestManager.startTest("Login With Admission Numbers VerIfication","");
			InfinityLearnLibrary.WebBrowser(testdata);
			
			sResult = InfinityLearnLibrary.LoginWithAdmission(testdata);
			if (sResult.equalsIgnoreCase("False")) {TearDown(); };
			
			sResult = InfinityLearnLibrary.Logout();
			if (sResult.equalsIgnoreCase("False")) {TearDown(); };
			
		}
		private void TearDown() throws Exception {
			throw new Exception("Test can't continue,  fail here!");
			}
		@AfterMethod
		public static void EndMethod() {
			ExtentManager.getReporter().flush();
			ExtentTestManager.endTest();
		}
		@AfterSuite
		public static void endSuite() {
			ExtentManager.getReporter().flush();
			ExtentTestManager.endTest();
		}
}

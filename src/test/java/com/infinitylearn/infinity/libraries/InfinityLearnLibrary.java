package com.infinitylearn.infinity.libraries;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.infinity.config.Config;
import com.infinitylearn.infinity.pageobjects.InfinityLearnObjects;
import com.infinitylearn.infinity.utils.DriverManager;
import com.infinitylearn.infinity.utils.LoggerUtil;
import com.infinitylearn.infinity.utils.WebInteractUtil;
import com.relevantcodes.extentreports.LogStatus;
import com.qa.infinitylearn.reporting.ExtentTestManager;
public class InfinityLearnLibrary {
	public WebDriver driver = DriverManager.WEB_DRIVER_THREAD.get();
	public InfinityLearnObjects InfinityLearnPage;
	public InfinityLearnLibrary()
	{
		InfinityLearnPage = new InfinityLearnObjects();	
	}
	public String WebBrowser(Map<String, String> testdata) throws Exception {
		
		//WebInteractUtil.launchWebApp(APP_URL);
		WebInteractUtil.launchWebApp(testdata.get("APP_URL"));

		return testdata.get("APP_URL");
		}
	public String HomePage(Map<String, String> testdata) throws Exception {
		String Status = null;	
		WebInteractUtil.Click(InfinityLearnPage.JoinFF1);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.signfrmJ1);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.PhoneTxt, 60); 
		WebInteractUtil.SendKeys(InfinityLearnPage.PhoneTxt, testdata.get("PhoneNumber"));
		WebInteractUtil.Click(InfinityLearnPage.PasswordBtn);
		WebInteractUtil.Click(InfinityLearnPage.PasswordTxb);
		WebInteractUtil.SendKeys(InfinityLearnPage.PasswordTxb, testdata.get("Password"));
		Thread.sleep(200);
		WebInteractUtil.Click(InfinityLearnPage.LoginBtn);
			if (WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.InfinityLTN, 60)) {
				LoggerUtil.printExtentLog("Pass", "Application login is successfull");
				Status = "True";
			}	else {	
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Application login is not successfull, Please Verify");
				Status ="False";
			}
		/*WebInteractUtil.Click(InfinityLearnPage.Coursesdropdwn);
		WebInteractUtil.Click(InfinityLearnPage.NEETBtn);
		WebInteractUtil.Waittilljquesryupdated();
		//((JavascriptExecutor)driver).executeScript("document.body.style.zoom='90%';");		//Zoom

		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ScheduleBtn, 60)){
			LoggerUtil.printExtentLog("Pass", "Navigation to Neet Page successfull");
			Status = "True";
		}else{
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigation to Neet page is unsuccessfull, Please Verify");
			Status ="False";
		}
		WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.Coursesdropdwn);
		WebInteractUtil.Click(InfinityLearnPage.JEEBtn);
		WebInteractUtil.Waittilljquesryupdated();
		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JEEpage, 60)) {
			LoggerUtil.printExtentLog("Pass", "Navigation to JEE Page successfull");
			Status = "True";
		}else{
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigation to JEE page is unsuccessfull, Please Verify");
			Status ="False";
		}	
		WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60);	
		WebInteractUtil.Click(InfinityLearnPage.AboutUs1);
		WebInteractUtil.Waittilljquesryupdated();
		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyAboutPage, 60)) {
			LoggerUtil.printExtentLog("Pass", "Navigation to About Us Page is successfull");
			Status = "True";
		}else{
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigation to About Us Page is not successfull, Please Verify");
			Status ="False";		
		}
		WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60);
		WebInteractUtil.Click(InfinityLearnPage.NeetExploreBtn);
		WebInteractUtil.Waittilljquesryupdated();
		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ScheduleBtn, 60)){
			LoggerUtil.printExtentLog("Pass", "Navigation to Neet Page successfull");
			Status = "True";
		}else{
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigation to Neet page is unsuccessfull, Please Verify");
			Status ="False";
		}	
		WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60);
		WebInteractUtil.Click(InfinityLearnPage.JeeExploreBtn);
		WebInteractUtil.Waittilljquesryupdated();
		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JEEpage, 60)) {
			LoggerUtil.printExtentLog("Pass", "Navigation to JEE Page successfull");
			//WebInteractUtil.Click(InfinityLearnPage.ViewTestScheduleBtn);
			//WebInteractUtil.Waittilljquesryupdated();
			//WebInteractUtil.Click(InfinityLearnPage.CancelXBtn);
			WebInteractUtil.Waittilljquesryupdated();
			Status = "True";
		}else{
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigation to JEE page is unsuccessfull, Please Verify");
			Status ="False";
		}
		WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60);
		WebInteractUtil.Click(InfinityLearnPage.NEETFooterBtn);
		WebInteractUtil.Waittilljquesryupdated();
		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ScheduleBtn, 60)){
			LoggerUtil.printExtentLog("Pass", "Navigation to Neet Page successfull");
			Status = "True";
		}else{
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigation to Neet page is unsuccessfull, Please Verify");
			Status ="False";
		}
		WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60);
		WebInteractUtil.Click(InfinityLearnPage.JEEFooterBtn);
		WebInteractUtil.Waittilljquesryupdated();
		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JEEpage, 60)) {
			LoggerUtil.printExtentLog("Pass", "Navigation to JEE Page successfull");
			Status = "True";
		}else{
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigation to JEE page is unsuccessfull, Please Verify");
			Status ="False";
		}
		WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60);
		WebInteractUtil.Click(InfinityLearnPage.AboutUsFooterBtn);
		WebInteractUtil.Waittilljquesryupdated();
		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyAboutPage, 60)) {
			LoggerUtil.printExtentLog("Pass", "Navigation to About Us Page is successfull");
			Status = "True";
		}else{
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigation to About Us Page is not successfull, Please Verify");
			Status ="False";		
		}
		WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60);
		WebInteractUtil.Click(InfinityLearnPage.TermsConditionsBtn);
		WebInteractUtil.Click(InfinityLearnPage.BackTermsconditionsBtn);*/		
		// considering that there is only one tab opened in that point.
	    String oldTab = DriverManager.WEB_DRIVER_THREAD.get().getWindowHandle();
		
		WebInteractUtil.Click(InfinityLearnPage.FaceBookLnk);
		Thread.sleep(5000);
//		switch to the upper window and verify the page and close the tab and move the control the home/default
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(tabs.get(1));
		WebInteractUtil.Waittilljquesryupdated();
		System.out.println(DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl());
		LoggerUtil.printExtentLog("Pass","Navigated to "+ DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl()+" URL by clicking FaceBook footer button");
		DriverManager.WEB_DRIVER_THREAD.get().close();
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(oldTab);
		
		WebInteractUtil.Click(InfinityLearnPage.LinkedInLnk);
		Thread.sleep(5000);
//		switch to the upper window and verify the page and close the tab and move the control the home/default
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(tabs1.get(1));
		WebInteractUtil.Waittilljquesryupdated();
		System.out.println(DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl());
		LoggerUtil.printExtentLog("Pass","Navigated to "+ DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl()+" URL by clicking Linked-IN footer button");
		DriverManager.WEB_DRIVER_THREAD.get().close();
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(oldTab);
		
		WebInteractUtil.Click(InfinityLearnPage.InstagramLnk);
		Thread.sleep(5000);
//		switch to the upper window and verify the page and close the tab and move the control the home/default
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(tabs2.get(1));
		WebInteractUtil.Waittilljquesryupdated();
		System.out.println(DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl());
		LoggerUtil.printExtentLog("Pass","Navigated to "+ DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl()+" URL by clicking Instragram footer button");
		DriverManager.WEB_DRIVER_THREAD.get().close();
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(oldTab);
		
		WebInteractUtil.Click(InfinityLearnPage.YoutubeLnk);
		Thread.sleep(5000);
//		switch to the upper window and verify the page and close the tab and move the control the home/default
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(tabs3.get(1));
		WebInteractUtil.Waittilljquesryupdated();
		System.out.println(DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl());
		LoggerUtil.printExtentLog("Pass","Navigated to "+ DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl()+" URL by clicking Youtube footer button");
		DriverManager.WEB_DRIVER_THREAD.get().close();
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(oldTab);
		
		WebInteractUtil.Click(InfinityLearnPage.TwitterLnk);
		Thread.sleep(5000);
//		switch to the upper window and verify the page and close the tab and move the control the home/default
		ArrayList<String> tabs4 = new ArrayList<String>(driver.getWindowHandles());
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(tabs4.get(1));
		WebInteractUtil.Waittilljquesryupdated();
		System.out.println(DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl());
		LoggerUtil.printExtentLog("Pass","Navigated to "+ DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl()+" URL by clicking Twitter footer button");
		DriverManager.WEB_DRIVER_THREAD.get().close();
		DriverManager.WEB_DRIVER_THREAD.get().switchTo().window(oldTab);
		WebInteractUtil.Waittilljquesryupdated();

		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.SigninBtn, 60)) {
			LoggerUtil.printExtentLog("Pass", "Login Page verification is successfull");
			Status = "True";
		}else {
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Login Page verification is not successfull, Please Verify");
			Status ="False";
		}
		return Status;
		}	
	public String LoginInfinity(Map<String, String> testdata) throws Exception{
		String Status = null;	
		
		WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60); 
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.SigninBtn);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.PhoneTxt, 60); 
		WebInteractUtil.SendKeys(InfinityLearnPage.PhoneTxt, testdata.get("PhoneNumber"));
		
		//WebInteractUtil.isPresent(InfinityLearnPage.PasswordBtn, 60);
		WebInteractUtil.Click(InfinityLearnPage.PasswordBtn);
		WebInteractUtil.Click(InfinityLearnPage.PasswordTxb);
		WebInteractUtil.SendKeys(InfinityLearnPage.PasswordTxb, testdata.get("Password"));
		Thread.sleep(200);
		//WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.LoginBtn, 60);
		WebInteractUtil.Click(InfinityLearnPage.LoginBtn);
			if (WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.InfinityLTN, 60)) {
				LoggerUtil.printExtentLog("Pass", "Application login is successfull");
				Status = "True";
			}	else {	
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Application login is not successfull, Please Verify");
				Status ="False";
			}
		return Status;
		}
	public String EditProfile(Map<String, String> testdata) throws Exception {			
		String Status = null;
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.ProfileIcon);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.HomeProfileBtn);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.VerifyProfilePage, 60);
		WebInteractUtil.Click(InfinityLearnPage.EditProfileBtn);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.EditGradeBtn);
		WebInteractUtil.Waittilljquesryupdated();
			switch(testdata.get("Grade")) {
			case "9":
				WebInteractUtil.Click(InfinityLearnPage.Grade9thBtn);
				WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
				WebInteractUtil.Waittilljquesryupdated();
				break;
			case "10":
				WebInteractUtil.Click(InfinityLearnPage.Grade10thBtn);
				WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
				WebInteractUtil.Waittilljquesryupdated();
				break;
			case "11":
				WebInteractUtil.Click(InfinityLearnPage.Grade11thBtn);
				WebInteractUtil.Click(InfinityLearnPage.PcmbStream);
				WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
				WebInteractUtil.Waittilljquesryupdated();
				break;
			case "12":
				WebInteractUtil.Click(InfinityLearnPage.Grade12thBtn);
				WebInteractUtil.Click(InfinityLearnPage.PcmbStream);
				WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
				WebInteractUtil.Waittilljquesryupdated();
				break;
			case "13":
				WebInteractUtil.Click(InfinityLearnPage.Grade13thBtn);
				WebInteractUtil.Click(InfinityLearnPage.PcmbStream);
				WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
				WebInteractUtil.Waittilljquesryupdated();
				break;}
		WebInteractUtil.Click(InfinityLearnPage.EditBoardBtn);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.APBoardBtn);
		WebInteractUtil.Click(InfinityLearnPage.SavePreferencesBtn);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.EDITTargetExamBtn);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(InfinityLearnPage.TargetNeetBtn,60);
		
	    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.NeetTargetSelected, 10)) {
	    	LoggerUtil.logConsoleMessage("Neet button is already enabled");
		}	else {	
			WebInteractUtil.Click(InfinityLearnPage.TargetNeetBtn);
			LoggerUtil.logConsoleMessage("Neet button is enabled");		}
	    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JeeMainTargetSelected, 10)) {
	    	LoggerUtil.logConsoleMessage("JEE Main button is already enabled");
		}	else {		
			WebInteractUtil.Click(InfinityLearnPage.TargetJeeBtn);
			LoggerUtil.logConsoleMessage("JEE Main button is enabled");}
	    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JeeAdvTargetSelected, 10)) {
	    	LoggerUtil.logConsoleMessage("JEE Advanced button is already enabled");
		}	else {		
			WebInteractUtil.Click(InfinityLearnPage.TargetJeeAdvancedBtn);
			LoggerUtil.logConsoleMessage("JEE Advanced button is enabled");}				

		WebInteractUtil.isPresent(InfinityLearnPage.SavePreferencesBtn, 60);
		WebInteractUtil.Click(InfinityLearnPage.SavePreferencesBtn);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
		WebInteractUtil.Waittilljquesryupdated();	
		WebInteractUtil.isPresent(InfinityLearnPage.ChangePasswordBtn, 60);	
		WebInteractUtil.Click(InfinityLearnPage.ChangePasswordBtn);
		WebInteractUtil.isPresent(InfinityLearnPage.VerifyChangePassword, 60);
		WebInteractUtil.SendKeys(InfinityLearnPage.CurrentPasswordTxb, testdata.get("Password"));
		WebInteractUtil.SendKeys(InfinityLearnPage.NewPasswordTxb, testdata.get("Password"));
		WebInteractUtil.SendKeys(InfinityLearnPage.ConfirmPasswordTxb, testdata.get("Password"));
		WebInteractUtil.Click(InfinityLearnPage.ConfirmPasswordBtn);
	    WebInteractUtil.Waittilljquesryupdated(); 
	    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyPasswordUpdated, 60)) {
	    	WebInteractUtil.Click(InfinityLearnPage.CloseBtn);
			WebInteractUtil.Waittilljquesryupdated();
			LoggerUtil.printExtentLog("Pass", "Changed password successfully");
			LoggerUtil.printExtentLog("Pass", "Edit Profile page is verified");
			Status = "True";
		}	else {
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Password change failed, Please check");
			LoggerUtil.printExtentLog("Pass", "Edit Profile page is verified");
			Status ="False";			
		}
	    WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
	    WebInteractUtil.Waittilljquesryupdated();
	    return Status;
		}	
		public String SetGradeTargtExam(Map<String, String> testdata) throws Exception{
			String Status = null;
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.ProfileIcon);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.HomeProfileBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.VerifyProfilePage, 60);
			WebInteractUtil.Click(InfinityLearnPage.EditProfileBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.EditGradeBtn);
			WebInteractUtil.Waittilljquesryupdated();
				switch(testdata.get("Grade")) {
				case "9":
					WebInteractUtil.Click(InfinityLearnPage.Grade9thBtn);
					WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
					WebInteractUtil.Waittilljquesryupdated();
					WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
					WebInteractUtil.Waittilljquesryupdated();
					break;
				case "10":
					WebInteractUtil.Click(InfinityLearnPage.Grade10thBtn);
					WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
					WebInteractUtil.Waittilljquesryupdated();
					WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
					WebInteractUtil.Waittilljquesryupdated();
					break;
				case "11":
					WebInteractUtil.Click(InfinityLearnPage.Grade11thBtn);
					WebInteractUtil.Click(InfinityLearnPage.PcmbStream);
					WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
					WebInteractUtil.Waittilljquesryupdated();
					WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
					WebInteractUtil.Waittilljquesryupdated();
					break;
				case "12":
					WebInteractUtil.Click(InfinityLearnPage.Grade12thBtn);
					WebInteractUtil.Click(InfinityLearnPage.PcmbStream);
					WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
					WebInteractUtil.Waittilljquesryupdated();
					WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
					WebInteractUtil.Waittilljquesryupdated();
					break;
				case "13":
					WebInteractUtil.Click(InfinityLearnPage.Grade13thBtn);
					WebInteractUtil.Click(InfinityLearnPage.PcmbStream);
					WebInteractUtil.Click(InfinityLearnPage.SaveDetailsBtn);
					WebInteractUtil.Waittilljquesryupdated();
					WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
					WebInteractUtil.Waittilljquesryupdated();
					break;}
			WebInteractUtil.Click(InfinityLearnPage.EditBoardBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.APBoardBtn);
			WebInteractUtil.Click(InfinityLearnPage.SavePreferencesBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.EDITTargetExamBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.TargetNeetBtn,60);
			
		    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.NeetTargetSelected, 10)) {
		    	LoggerUtil.logConsoleMessage("Neet button is already enabled");
			}	else {	
				WebInteractUtil.Click(InfinityLearnPage.TargetNeetBtn);
				LoggerUtil.logConsoleMessage("Neet button is enabled");		}
		    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JeeMainTargetSelected, 10)) {
		    	LoggerUtil.logConsoleMessage("JEE Main button is already enabled");
			}	else {		
				WebInteractUtil.Click(InfinityLearnPage.TargetJeeBtn);
				LoggerUtil.logConsoleMessage("JEE Main button is enabled");}
		    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JeeAdvTargetSelected, 10)) {
		    	LoggerUtil.logConsoleMessage("JEE Advanced button is already enabled");
			}	else {		
				WebInteractUtil.Click(InfinityLearnPage.TargetJeeAdvancedBtn);
				LoggerUtil.logConsoleMessage("JEE Advanced button is enabled");}				

			WebInteractUtil.isPresent(InfinityLearnPage.SavePreferencesBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.SavePreferencesBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		//
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.GoBackB);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.GoBackB);
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyLogin, 60)) {
				LoggerUtil.printExtentLog("Pass", "Grade and Target exam updated successfully");
				Status = "True";}else
				{
					LoggerUtil.printExtentLog("Fail", "Unable to update Grade, Target exam and navigate to dashboard, Please check");
					Status = "false";}
			return Status;}
			
		public String NavigateToAsessment() throws Exception{
			String Status = null;
			//WebInteractUtil.Click(InfinityLearnPage.AttemptTstOpenLst);
				WebInteractUtil.Click(InfinityLearnPage.AttemptNew);
				WebInteractUtil.Waittilljquesryupdated();
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.MockTst, 30)) {
				WebInteractUtil.Click(InfinityLearnPage.AttemptAnTst);	
				WebInteractUtil.Waittilljquesryupdated();
				LoggerUtil.printExtentLog("Pass", "Navigate to Mock Test List page is successfull");
				Status = "True";}
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyInstPage, 30)) {
				LoggerUtil.printExtentLog("Pass", "Navigate to General instructions page is successfull");
				Status = "True";}else
				{
					LoggerUtil.printExtentLog("Fail", "Navigate to General instructions page is not successfull, Please check");
					Status = "false";}
			return Status;}
			
			public String BeginWalkThrough() throws Exception  
			{	String Status = null;
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", InfinityLearnPage.BeginWalkThroughBtn);
				WebInteractUtil.Click(InfinityLearnPage.BeginWalkThroughBtn);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.isPresent(InfinityLearnPage.VerifyBeginWalk, 60);
				WebInteractUtil.Click(InfinityLearnPage.ClickNextBtn1);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.isPresent(InfinityLearnPage.Verfypage2, 60);
				WebInteractUtil.Click(InfinityLearnPage.ClickNextBtn1);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.isPresent(InfinityLearnPage.Verfypage3, 60);
				WebInteractUtil.Click(InfinityLearnPage.ClickNextBtn1);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.isPresent(InfinityLearnPage.BackToInstPageBtn, 60);
				WebInteractUtil.Click(InfinityLearnPage.BackToInstPageBtn);
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyInstPage, 60)) {
					LoggerUtil.printExtentLog("Pass", "BeginWalkThrough page verification is successfull");
					Status = "True";
				}	else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "BeginWalkThrough verification page is not successfull, Please Verify");
					Status ="False";
				}
				return Status;			
			}					
		public String CompleteTest() throws Exception
		{
			String Status = null;
			WebInteractUtil.isPresent(InfinityLearnPage.BegintstBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.BegintstBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.FirstQstn, 60);
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.Numpad,10)) {
				WebInteractUtil.Click(InfinityLearnPage.Num1);}	
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ClickRadioBtn1,10)) {
				WebInteractUtil.Click(InfinityLearnPage.ClickRadioBtn1);}		
			WebInteractUtil.Click(InfinityLearnPage.MarkForReview);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.SecondQstn, 60);
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.Numpad,10)) {
				WebInteractUtil.Click(InfinityLearnPage.Num1);}
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ClickRadioBtn1,10)) {
				WebInteractUtil.Click(InfinityLearnPage.ClickRadioBtn1);}
			WebInteractUtil.isPresent(InfinityLearnPage.ClearResps, 60);
			WebInteractUtil.Click(InfinityLearnPage.ClearResps);
			WebInteractUtil.isPresent(InfinityLearnPage.FifthQbtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.FifthQbtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.FifthQstn, 60);
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.Numpad,20)) {
				WebInteractUtil.Click(InfinityLearnPage.Num1);}
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ClickRadioBtn1, 10)) {	
				WebInteractUtil.Click(InfinityLearnPage.ClickRadioBtn1);}
				WebInteractUtil.Click(InfinityLearnPage.SaveBtn);
				WebInteractUtil.Waittilljquesryupdated();
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.SecBBtn, 20)) {
			WebInteractUtil.Click(InfinityLearnPage.SecBBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.ForwardArrowBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.ForwardArrowBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.BackWardArrowBtn);
			WebInteractUtil.Click(InfinityLearnPage.ChemistryBtn);
	//		WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ClickRadioBtn1, 60);
	//		WebInteractUtil.Click(InfinityLearnPage.ClickRadioBtn1);
	//		WebInteractUtil.Click(InfinityLearnPage.MarkForReview);
			WebInteractUtil.Waittilljquesryupdated();
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			WebInteractUtil.isPresent(InfinityLearnPage.SecBBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.SecBBtn);
	 		WebInteractUtil.isPresent(InfinityLearnPage.BiologyBtn,60); 
	 		WebInteractUtil.Click(InfinityLearnPage.BiologyBtn);
			WebInteractUtil.Click(InfinityLearnPage.SecBBtn);}
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
	 		if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.MathsBtn,10)) {
		 		WebInteractUtil.Click(InfinityLearnPage.MathsBtn);}
	//		WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ClickRadioBtn1, 60);
	//		WebInteractUtil.Click(InfinityLearnPage.ClickRadioBtn1);
	//		WebInteractUtil.Click(InfinityLearnPage.MarkForReview);
			WebInteractUtil.Waittilljquesryupdated();
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			WebInteractUtil.isPresent(InfinityLearnPage.ChemistryBtn,10);
			WebInteractUtil.Click(InfinityLearnPage.ChemistryBtn);
			/*if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.BiologyBtn,60)) {
		 		WebInteractUtil.Click(InfinityLearnPage.BiologyBtn);
			}*/
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.MathsBtn,10)) {
		 		WebInteractUtil.Click(InfinityLearnPage.MathsBtn);}
			// General Instructions
			WebInteractUtil.Click(InfinityLearnPage.GeneralInst);
			WebInteractUtil.Click(InfinityLearnPage.GeneralInstBtn);
			WebInteractUtil.Click(InfinityLearnPage.GoBackBtn);
			LoggerUtil.printExtentLog("Pass", "General Instructions page in assessment verification is successfull");
			// Question List
			WebInteractUtil.isPresent(InfinityLearnPage.QuestionLst, 60);
			WebInteractUtil.Click(InfinityLearnPage.QuestionLst);
			WebInteractUtil.Click(InfinityLearnPage.QuestionLstBtn);
			WebInteractUtil.Waittilljquesryupdated();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", InfinityLearnPage.Page2QstLst);
			WebInteractUtil.Click(InfinityLearnPage.Page2QstLst);
			/*((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", InfinityLearnPage.Page3QstLst);
			WebInteractUtil.Click(InfinityLearnPage.Page3QstLst);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", InfinityLearnPage.Page4QstLst);
			WebInteractUtil.Click(InfinityLearnPage.Page4QstLst);*/
			WebInteractUtil.Waittilljquesryupdated();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", InfinityLearnPage.Page1QstLst);
			WebInteractUtil.Click(InfinityLearnPage.Page1QstLst);
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			WebInteractUtil.Click(InfinityLearnPage.GoToQstn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.QuestionLst, 60);
			WebInteractUtil.Click(InfinityLearnPage.QuestionLst);
			WebInteractUtil.Click(InfinityLearnPage.QuestionLstBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.QuestionLst, 60);
			WebInteractUtil.Click(InfinityLearnPage.QuestionLst);
			WebInteractUtil.Click(InfinityLearnPage.QuestionLstBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.NtAnsChkBx);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.AnsChkBx);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.NtVisitCkBx);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.NansRw);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.AnsRw);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.ChemistryBtn);
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.SecBBtn, 10)) {
			WebInteractUtil.Click(InfinityLearnPage.SecBBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.SecBBtn);
			WebInteractUtil.Waittilljquesryupdated();}
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.MathsBtn, 10)) {
				WebInteractUtil.Click(InfinityLearnPage.MathsBtn);}	
			WebInteractUtil.Click(InfinityLearnPage.BackToTestBtn);
			LoggerUtil.printExtentLog("Pass", "Question List page in assessment verification is successfull");
	//		WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.GoBackBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.ChemistryBtn);
			WebInteractUtil.isPresent(InfinityLearnPage.SubmitBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.SubmitBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.BackToTestSubmtBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.SubmitBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.FinishTstBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.FinishTstBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.MediumBtn);
			WebInteractUtil.Click(InfinityLearnPage.BadBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.FinalSubmitBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.FinalSubmitBtn);
			WebInteractUtil.Waittilljquesryupdated();	
			WebInteractUtil.isPresent(InfinityLearnPage.CloseBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.CloseBtn);
			WebInteractUtil.Waittilljquesryupdated();	
			// Test Completion					
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ViewSolnBtn, 60)) {
				LoggerUtil.printExtentLog("Pass", "Assessment completed successfully");
				Status = "True";
			}	else {
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Assessment is not successfully completed, Please Verify");
				Status ="False";
			}	
			WebInteractUtil.Click(InfinityLearnPage.PhyViewSoltnBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.TestSolnsBckBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.ChemViewSoltnBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.TestSolnsBckBtn);
			WebInteractUtil.Waittilljquesryupdated();
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.BioViewSoltnBtn,10)) {
				WebInteractUtil.Click(InfinityLearnPage.BioViewSoltnBtn);
			}else {
				WebInteractUtil.Click(InfinityLearnPage.MathViewSoltnBtn);
			}
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.TestSolnsBckBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.ViewSolnBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.ExtremeRightBtn);
			WebInteractUtil.Click(InfinityLearnPage.ChemistryBtn);
			/*if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.BiologyBtn,10)) {
				WebInteractUtil.Click(InfinityLearnPage.BiologyBtn);
			}else {
				WebInteractUtil.Click(InfinityLearnPage.MathsBtn);
			}*/
			WebInteractUtil.Click(InfinityLearnPage.ExtremeLeftBtn);
			WebInteractUtil.Click(InfinityLearnPage.TestSolnsBckBtn);
			LoggerUtil.printExtentLog("Pass", "View Full Solution page verification is successfull");
				return Status;
			}	
		// Previous Papers Test
		public String NavigatePrevTstPage() throws Exception  	
		{
			String Status = null;
			WebInteractUtil.Click(InfinityLearnPage.PrePaperOpenList);
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyPrePaprPage, 60)) {
				LoggerUtil.printExtentLog("Pass", "Navigate to Test List page is successfull");
								Status = "True";
			}
			else {
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigate to Test List is not successfull, Please Verify");
				Status ="False";
			}	
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.PrePaperAttemptNw, 20)) {
				WebInteractUtil.Click(InfinityLearnPage.PrePaperAttemptNw);}
			else {
				WebInteractUtil.Click(InfinityLearnPage.NextPageBtn);
				WebInteractUtil.Waittilljquesryupdated();
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.PrePaperAttemptNw, 20)) {
				WebInteractUtil.Click(InfinityLearnPage.PrePaperAttemptNw);}
				else {WebInteractUtil.Click(InfinityLearnPage.NextPageBtn);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.isPresent(InfinityLearnPage.PrePaperAttemptNw, 30);
				WebInteractUtil.Click(InfinityLearnPage.PrePaperAttemptNw);}
			}
			WebInteractUtil.Waittilljquesryupdated();
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyInstPage, 30)) {
				LoggerUtil.printExtentLog("Pass", "Navigate to General instructions page is successfull");
				Status = "True";}else
				{
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigate to General instructions page is not successfull, Please Check");
					Status = "false";}
			return Status;}
		//	Report after Login
		public String ReportPage() throws Exception {
			String Status = null;
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ViewDtldRptBtn, 60)) {
		//	WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyMyRpt, 60);
			WebInteractUtil.Click(InfinityLearnPage.ViewDtldRptBtn);
			WebInteractUtil.Click(InfinityLearnPage.Days15Btn);
			WebInteractUtil.Click(InfinityLearnPage.Month1Btn);
			WebInteractUtil.Click(InfinityLearnPage.Month6Btn);
			WebInteractUtil.Click(InfinityLearnPage.Year1Btn);
			WebInteractUtil.Click(InfinityLearnPage.Week1Btn);
		/*	WebInteractUtil.Click(InfinityLearnPage.DetailedRptDrp);
			WebInteractUtil.Click(InfinityLearnPage.InfiLrnTstSrsBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.InfiLrnTstDrpDwn);
			WebInteractUtil.Click(InfinityLearnPage.MockTestBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.MockTstDrpDwn);
			WebInteractUtil.Click(InfinityLearnPage.PreYerPapBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.PrevYerPapDrpDwn);
			WebInteractUtil.Click(InfinityLearnPage.AllTstBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.OverAllDrpDwn);
			WebInteractUtil.Click(InfinityLearnPage.TimeTakenBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.TimeTakenDrpDwn);
			WebInteractUtil.Click(InfinityLearnPage.OverAllBtn);*/
			WebInteractUtil.Waittilljquesryupdated();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", InfinityLearnPage.ChemistryViewRpt);
			WebInteractUtil.Click(InfinityLearnPage.ChemistryViewRpt);
			WebInteractUtil.Waittilljquesryupdated();
			Actions action = new Actions(driver);
			action.sendKeys(Keys.ESCAPE).build().perform();
			WebInteractUtil.Click(InfinityLearnPage.PhysicsViewRpt);
			WebInteractUtil.Waittilljquesryupdated();
			action.sendKeys(Keys.ESCAPE).build().perform();
			WebInteractUtil.Click(InfinityLearnPage.BiologyViewRpt);
			WebInteractUtil.Waittilljquesryupdated();
			action.sendKeys(Keys.ESCAPE).build().perform();
			WebElement html = driver.findElement(By.tagName("html"));
			html.sendKeys(Keys.chord(Keys.CONTROL, Keys.ADD));
			
				//((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");

			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.InfinityLogo, 60)) {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", InfinityLearnPage.InfinityLogo);

				//WebInteractUtil.Click(InfinityLearnPage.RptGoBckBtn);
				LoggerUtil.printExtentLog("Pass", "Reports page verification is successfull");
								Status = "True";
			}else {
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Reports page verification is not successfull, Please Check");
				Status ="False";
			}}else
			{LoggerUtil.printExtentLog("Pass", "No reports to verify, Please complete any assessment and run the script again");
			Status = "True";}

			return Status;
		}	
		public String InfinityTestSeries() throws Exception {
			String Status = null;
			WebInteractUtil.Click(InfinityLearnPage.TestSeriesOpenLstBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.GridView);
			WebInteractUtil.Click(InfinityLearnPage.ListView);
			WebInteractUtil.Click(InfinityLearnPage.FilterBtn);
			WebInteractUtil.Click(InfinityLearnPage.AttemptedBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.FilterBtn);
			WebInteractUtil.Click(InfinityLearnPage.UnattemptedBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.FilterBtn);
			WebInteractUtil.Click(InfinityLearnPage.ClearBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.TestSeriesBackBtn);
			WebInteractUtil.Waittilljquesryupdated();
			if (WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.InfinityLTN, 60)) {
				LoggerUtil.printExtentLog("Pass", "Filters, List View and Grid View are working fine");
				Status = "True";
			}
			else {	
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Filters, List View and Grid View are not working as expected, Please check");
				Status ="False";
			}
			return Status;
		}
		public String Logout() throws Exception
		{
			String Status = null;
			WebInteractUtil.isPresent(InfinityLearnPage.ProfileIcon, 60);
			WebInteractUtil.Click(InfinityLearnPage.ProfileIcon);
			WebInteractUtil.isPresent(InfinityLearnPage.LogoutBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.LogoutBtn);
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.SigninBtn, 60)) {
				LoggerUtil.printExtentLog("Pass", "Logout is successfull");
				Status = "True";
			}
			else {
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Logout is not successfull, Please Verify");
				Status ="False";
			}	
			return Status;
			}
		public String OTPSignIn(Map<String, String> testdata) throws Exception {
			String Status = null;
			
			WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60); 
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.SigninBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.PhoneTxt, 60); 
			WebInteractUtil.SendKeys(InfinityLearnPage.PhoneTxt, testdata.get("PhoneNumber"));
			WebInteractUtil.Click(InfinityLearnPage.GetOTPBtn);						
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyLogin, 120)) {
				LoggerUtil.printExtentLog("Pass", "OTP Login Successfull");
				Thread.sleep(1000);
				Status = "True";	
			} else {
				LoggerUtil.printExtentLogWithScreenshot("Fail", "OTP Login is not successfull, Please check");
				Status ="False";}
			return Status;
		}
		/*public String OTPNewUser(Map<String, String> testdata) throws Exception {
			String Status = null;
			WebInteractUtil.isPresent(InfinityLearnPage.PhoneTxt, 60); 
			WebInteractUtil.SendKeys(InfinityLearnPage.PhoneTxt, testdata.get("PhoneNumber"));
			WebInteractUtil.Click(InfinityLearnPage.StartBtn);
			WebInteractUtil.Waittilljquesryupdated();

			WebInteractUtil.SendKeys(InfinityLearnPage.FirstNameTxtB, testdata.get("Register_Firstname"));
			WebInteractUtil.SendKeys(InfinityLearnPage.LastNameTxtB, testdata.get("Register_Lastname"));
			WebInteractUtil.Click(InfinityLearnPage.WhatsappChkB);
			WebInteractUtil.Click(InfinityLearnPage.RegisterBtn);
			WebInteractUtil.Waittilljquesryupdated();
			//public static String phoneNumber = "9573140740";
			//public static String phoneNumber = "9299855610";
			//public static String phoneNumber = "9492416719";
			//public static String phoneNumber = "9133434966";
			//public static String phoneNumber = "8328262039";
			//public static String phoneNumber = "9121605385";
			//public static String password = "123456";
			/*WebInteractUtil.Click(InfinityLearnPage.EditBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.SendKeys(InfinityLearnPage.EditPhone, Config.phoneNumber);
			WebInteractUtil.Click(InfinityLearnPage.EditNxtBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.SendKeys(InfinityLearnPage.FirstNameTxtB, Config.firstName);
			WebInteractUtil.SendKeys(InfinityLearnPage.LastNameTxtB, Config.lastName);
			WebInteractUtil.Click(InfinityLearnPage.WhatsappChkB);
			WebInteractUtil.Click(InfinityLearnPage.RegisterBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.SendKeys(InfinityLearnPage.OtpTxtB, Config.otp);
			WebInteractUtil.isPresent(InfinityLearnPage.VerifyOtpBtn, 60); 
			WebInteractUtil.Click(InfinityLearnPage.VerifyOtpBtn);
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyAccountCrtd, 120)) {
				LoggerUtil.printExtentLog("Pass", "Login is successfull");
				Status = "True";	
			} else {
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Login is not successfull, Please Verify");
				Status ="False";
				}	
			WebInteractUtil.Click(InfinityLearnPage.StartOnBrdBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.SendKeys(InfinityLearnPage.EmailTxtB, testdata.get("Email_ID"));
			WebInteractUtil.SendKeys(InfinityLearnPage.SetPasswordTxtB, testdata.get("Register_Firstname"));
			WebInteractUtil.SendKeys(InfinityLearnPage.SetConfirmPassTxtB, testdata.get("Register_Lastname"));
			WebInteractUtil.Click(InfinityLearnPage.SetUpAccNextBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.Grade12thBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.PcmbStream);
			WebInteractUtil.Waittilljquesryupdated();	
			WebInteractUtil.Click(InfinityLearnPage.TargetJeeAdvancedBtn);
			WebInteractUtil.Click(InfinityLearnPage.TargetJeeBtn);
			WebInteractUtil.Click(InfinityLearnPage.TargetNeetBtn);
			WebInteractUtil.Click(InfinityLearnPage.GetStartedBtn);
			WebInteractUtil.Waittilljquesryupdated();
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyLogin, 120)) {
				LoggerUtil.printExtentLog("Pass", "User Registration is Successfull");
				Thread.sleep(1000);
				Status = "True";	
			} else {
				LoggerUtil.printExtentLogWithScreenshot("Fail", "User Registration is not successfull, Please check");
				Status ="False";}
			return Status;			
		}*/
		public String ForgotPassword(Map<String, String> testdata) throws Exception {
			String Status = null;
			
			WebInteractUtil.isPresent(InfinityLearnPage.SigninBtn, 60); 
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.SigninBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.PhoneTxt, 60); 
			WebInteractUtil.SendKeys(InfinityLearnPage.PhoneTxt, testdata.get("PhoneNumber"));
			WebInteractUtil.Click(InfinityLearnPage.PasswordBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.ForgotPassBtn,60);
			WebInteractUtil.Click(InfinityLearnPage.ForgotPassBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.SendOTPBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.SetPasswordTxtB, 120);					
			WebInteractUtil.SendKeys(InfinityLearnPage.SetPasswordTxtB, testdata.get("Password"));		
			WebInteractUtil.SendKeys(InfinityLearnPage.SetConfirmPassTxtB, testdata.get("Password"));	
			WebInteractUtil.Click(InfinityLearnPage.ResetPassBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.PhoneTxt, 60); 
			WebInteractUtil.SendKeys(InfinityLearnPage.PhoneTxt, testdata.get("PhoneNumber"));
			
			//WebInteractUtil.isPresent(InfinityLearnPage.PasswordBtn, 60);
			//WebInteractUtil.Click(InfinityLearnPage.PasswordBtn);
			//WebInteractUtil.Click(InfinityLearnPage.PasswordTxb);
			WebInteractUtil.SendKeys(InfinityLearnPage.PasswordTxb, testdata.get("Password"));
			Thread.sleep(200);
			WebInteractUtil.Click(InfinityLearnPage.LoginBtn);
			WebInteractUtil.Waittilljquesryupdated();
			if (WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.InfinityLTN, 60)) {
				LoggerUtil.printExtentLog("Pass", "Password Reset and Application login is successfull");
				Status = "True";
			} else {	
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Password Reset and Application login is not successfull, Please Verify");
				Status ="False";
			}
		return Status;
		}	
		
		public String EditProfileTC() throws Exception {
			String Status = null;
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.ProfileIcon);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.HomeProfileBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.isPresent(InfinityLearnPage.VerifyProfilePage, 60);
			WebInteractUtil.Click(InfinityLearnPage.EditProfileBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.EDITTargetExamBtn);
			WebInteractUtil.Waittilljquesryupdated();
			//WebInteractUtil.Click(InfinityLearnPage.TargetJeeAdvancedBtn);
			//WebInteractUtil.Click(InfinityLearnPage.TargetJeeBtn);
			WebInteractUtil.isPresent(InfinityLearnPage.TargetNeetBtn,60);
			
		    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.NeetTargetSelected, 10)) {
		    	LoggerUtil.logConsoleMessage("Neet button is already enabled");
			}	else {	
				WebInteractUtil.Click(InfinityLearnPage.TargetNeetBtn);
				LoggerUtil.logConsoleMessage("Neet button is enabled");		}
		    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JeeMainTargetSelected, 10)) {
		    	LoggerUtil.logConsoleMessage("JEE Main button is already enabled");
			}	else {		
				WebInteractUtil.Click(InfinityLearnPage.TargetJeeBtn);
				LoggerUtil.logConsoleMessage("JEE Main button is enabled");}
		    if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JeeAdvTargetSelected, 10)) {
		    	LoggerUtil.logConsoleMessage("JEE Advanced button is already enabled");
			}	else {		
				WebInteractUtil.Click(InfinityLearnPage.TargetJeeAdvancedBtn);
				LoggerUtil.logConsoleMessage("JEE Advanced button is enabled");}				

		/*	WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.SavePreferencesBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.SavePreferencesBtn);
			WebInteractUtil.Waittilljquesryupdated();	
			WebInteractUtil.Click(InfinityLearnPage.FinalSaveDetailsBtn);
			WebInteractUtil.isPresent(InfinityLearnPage.VerifyProfileUpdted, 60);
			WebInteractUtil.Click(InfinityLearnPage.CloseBtn);
			WebInteractUtil.Waittilljquesryupdated();
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			WebInteractUtil.Click(InfinityLearnPage.EditProfileBtn);
			WebInteractUtil.Click(InfinityLearnPage.EDITTargetExamBtn);
			WebInteractUtil.Click(InfinityLearnPage.TargetJeeAdvancedBtn);
			WebInteractUtil.Click(InfinityLearnPage.CancelgradeBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.EDITTargetExamBtn);
			WebInteractUtil.Click(InfinityLearnPage.TargetJeeBtn);
			WebInteractUtil.Click(InfinityLearnPage.TargetNeetBtn);
			WebInteractUtil.Click(InfinityLearnPage.TargetJeeAdvancedBtn);
            if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.NeetTargetSelected, 15)) {
                LoggerUtil.logConsoleMessage("Neet button is already enabled");
            }   else {
                WebInteractUtil.Click(InfinityLearnPage.TargetNeetBtn);
                LoggerUtil.logConsoleMessage("Neet button is clicked");}
			WebInteractUtil.isPresent(InfinityLearnPage.SavePreferencesBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.SavePreferencesBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.FinalSaveDetailsBtn);
			WebInteractUtil.Waittilljquesryupdated();
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			WebInteractUtil.Click(InfinityLearnPage.EditProfileBtn);*/
		    WebInteractUtil.isPresent(InfinityLearnPage.SavePreferencesBtn, 60);
			WebInteractUtil.Click(InfinityLearnPage.SavePreferencesBtn);
			WebInteractUtil.Waittilljquesryupdated();
			WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		
	//		WebInteractUtil.Click(InfinityLearnPage.FinalSaveDetailsBtn);
	//		WebInteractUtil.Waittilljquesryupdated();
	//		WebInteractUtil.isPresent(InfinityLearnPage.VerifyProfileUpdted, 60);
	//		WebInteractUtil.Click(InfinityLearnPage.CloseBtn);		
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ProfileIcon, 60)) {
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.isPresent(InfinityLearnPage.ProfileIcon, 60);
				WebInteractUtil.Click(InfinityLearnPage.ProfileIcon);
				WebInteractUtil.isPresent(InfinityLearnPage.LogoutBtn, 60);
				WebInteractUtil.Click(InfinityLearnPage.LogoutBtn);
				LoggerUtil.printExtentLog("Pass", "All possible test cases for Edit Profile are verified successfull");
				Status = "True";
			} else {	
				LoggerUtil.printExtentLogWithScreenshot("Fail", "Could not verify all possible test cases for Edit Profile, Please check");
				Status ="False";
			}
			return Status;
		}
		public String MockRptTC() throws Exception {
			String Status = null;
			WebInteractUtil.Click(InfinityLearnPage.AttemptTstOpenLst);
			WebInteractUtil.Waittilljquesryupdated();
			if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.MockTst, 30)) {
				WebInteractUtil.Click(InfinityLearnPage.GridView);
				WebInteractUtil.Click(InfinityLearnPage.ListView);
				WebInteractUtil.Click(InfinityLearnPage.MockTst);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.Click(InfinityLearnPage.AttemptTstOpenLst);
				LoggerUtil.printExtentLog("Pass", "Navtigated to Mock Test List page");
				WebInteractUtil.Waittilljquesryupdated();	
				Status = "True";}
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyInstPage, 30)) {
					LoggerUtil.printExtentLog("Pass", "Navigate to General instructions page is successfull");
					Status = "True";}
					return Status;}
		public String JEEMain() throws Exception {
			String Status = null;
			return Status;

		}
		 public String NavigatetoJEEMains() throws Exception
		 {
			 String Status = null;
				WebInteractUtil.Click(InfinityLearnPage.JeeButton);
				WebInteractUtil.Waittilljquesryupdated();
			   if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JEEMainPageVerify, 60)) 
			   {
	                LoggerUtil.printExtentLog("Pass", "Navigate to JEE Main page is successfull");
	                Status = "True";        
			   }  else	{
	              LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigate to JEE Main page is not successfull, Please Check");
	                    Status = "False";
	              }
	            return Status;
	            }
		
		 public String LiveClassesPage() throws Exception {

				String Status = null;
				WebInteractUtil.Click(InfinityLearnPage.LiveClsMdleBtn);
				LoggerUtil.printExtentLog("Pass", "Navigated to Live class");
				Status = "True";
				return Status;
			}
		 public String LoginWithAdmission(Map<String, String> testdata) throws Exception {

				String Status = null;
				WebInteractUtil.isPresent(InfinityLearnPage.SCClickBtn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.SCClickBtn); 
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.AdmissionNoTxb, 60))
				{
					System.out.println(DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl());
					LoggerUtil.printExtentLog("Pass","Navigated to "+ DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl()+" URL by clicking Sri Chaitanya students click here to login button");
					Status = "True";
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Unable to Navigate to "+"SC login page by clicking Sri Chaitanya students click here to login button , Please Check");
                    Status = "False";
				}
				WebInteractUtil.Click(InfinityLearnPage.AdmissionNoTxb); 
				WebInteractUtil.SendKeys(InfinityLearnPage.AdmissionNoTxb, testdata.get("AdmissionNo"));
				WebInteractUtil.Click(InfinityLearnPage.StartBtn);
				WebInteractUtil.isPresent(InfinityLearnPage.AdPaswd, 60);
				WebInteractUtil.Click(InfinityLearnPage.AdPaswd);
				WebInteractUtil.SendKeys(InfinityLearnPage.AdPaswd, testdata.get("StudentPassword"));
				Thread.sleep(200);
				WebInteractUtil.Click(InfinityLearnPage.LoginBtn);
				WebInteractUtil.Waittilljquesryupdated();
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyFinal, 10)) {
					LoggerUtil.printExtentLog("Pass", "Login is successfull for admission number - "+testdata.get("AdmissionNo"));
					Status = "True";		
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Login is Unsuccessfull for admission number "+testdata.get("AdmissionNo")+" , Please Check");
                    Status = "False";
				}
				WebInteractUtil.Click(InfinityLearnPage.FinalOpenList);
				WebInteractUtil.Waittilljquesryupdated();
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.ExamPage, 60)) {
					LoggerUtil.printExtentLog("Pass", "Navigated to Final Infinity National Learners Exam (FINALE) page");
					Status = "True";
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigated to Final Infinity National Learners Exam (FINALE) page is Unsuccessfull, Please Check");
                    Status = "False";
				}
				WebInteractUtil.Click(InfinityLearnPage.ExamPage);
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VerifyFinal, 60)) {
					LoggerUtil.printExtentLog("Pass", "Back to Homepage is successfull");
					Status = "True";		
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Back to Homepage is Unsuccessfull, Please Check");
                    Status = "False";
				}				
				return Status;
		 }
		 public String ScholarshipTest(Map<String, String> testdata) throws Exception {

				String Status = null;
				WebInteractUtil.isPresent(InfinityLearnPage.schlshpdrpdwn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.schlshpdrpdwn);
				WebInteractUtil.isPresent(InfinityLearnPage.Ilitebtn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.Ilitebtn);
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.NSyllabusBtn, 60)) {
					LoggerUtil.printExtentLog("Pass", "Navigated to Scholarship Test page");
					Status = "True";
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Unable to navigate to Scholarship Test page, Please Check");
                    Status = "False";}				
				WebInteractUtil.isPresent(InfinityLearnPage.NSyllabusBtn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.NSyllabusBtn);
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VrfySyllabus, 60)) {
					LoggerUtil.printExtentLog("Pass", "Able to see Target NEET syllabus");
					Status = "True";
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Unable to see Target NEET syllabus, Please Check");
                    Status = "False";}				
				Actions action = new Actions(driver);
				action.sendKeys(Keys.ESCAPE).build().perform();
				WebInteractUtil.Click(InfinityLearnPage.JMBtn);
				WebInteractUtil.isPresent(InfinityLearnPage.JMSyllabusBtn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.JMSyllabusBtn);
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VrfySyllabus, 60)) {
					LoggerUtil.printExtentLog("Pass", "Able to see Target JEE Main syllabus");
					Status = "True";
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Unable to see Target JEE Main syllabus, Please Check");
                    Status = "False";}
				action.sendKeys(Keys.ESCAPE).build().perform();
				WebInteractUtil.Click(InfinityLearnPage.JABtn);
				WebInteractUtil.isPresent(InfinityLearnPage.JASyllabusBtn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.JASyllabusBtn);
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VrfySyllabus, 60)) {
					LoggerUtil.printExtentLog("Pass", "Able to see Target JEE Advanced syllabus");
					Status = "True";
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Unable to see Target JEE Advanced syllabus, Please Check");
                    Status = "False";}
				action.sendKeys(Keys.ESCAPE).build().perform();
				WebInteractUtil.isPresent(InfinityLearnPage.NBtn1, 60); 
				WebInteractUtil.isPresent(InfinityLearnPage.TermsBtn, 60);
				WebInteractUtil.Click(InfinityLearnPage.TermsBtn);
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.VrfyTerms, 60)) {
					LoggerUtil.printExtentLog("Pass", "Able to see Terms and conditions page");
					Status = "True";
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Unable to see Terms and conditions page, Please Check");
                    Status = "False";}
				WebInteractUtil.Waittilljquesryupdated();
				action.sendKeys(Keys.ESCAPE).build().perform();
				WebInteractUtil.isPresent(InfinityLearnPage.JBtn, 60);
				WebInteractUtil.Click(InfinityLearnPage.JBtn);
				WebInteractUtil.isPresent(InfinityLearnPage.NBtn2, 60);
				WebInteractUtil.Click(InfinityLearnPage.NBtn2);
				WebInteractUtil.isPresent(InfinityLearnPage.WhatsappBtn, 60);
				WebInteractUtil.Click(InfinityLearnPage.WhatsappBtn);
				System.out.println(DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl());
				LoggerUtil.printExtentLog("Pass","Navigated to "+ DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl()+" URL by clicking Whatsapp button");
				WebInteractUtil.Waittilljquesryupdated();
				driver.navigate().back();
				WebInteractUtil.isPresent(InfinityLearnPage.schlshpdrpdwn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.schlshpdrpdwn);
				WebInteractUtil.isPresent(InfinityLearnPage.Ilitebtn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.Ilitebtn);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.isPresent(InfinityLearnPage.FaceBookLnk, 60);
				WebInteractUtil.Click(InfinityLearnPage.FaceBookLnk);
				System.out.println(DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl());
				LoggerUtil.printExtentLog("Pass","Navigated to "+ DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl()+" URL by clicking FaceBook button");
				WebInteractUtil.Waittilljquesryupdated();
				driver.navigate().back();
				WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.schlshpdrpdwn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.schlshpdrpdwn);
				WebInteractUtil.isPresent(InfinityLearnPage.Ilitebtn, 60); 
				WebInteractUtil.Click(InfinityLearnPage.Ilitebtn);
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.isPresent(InfinityLearnPage.InstagramLnk, 60);
				WebInteractUtil.Click(InfinityLearnPage.InstagramLnk);
				System.out.println(DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl());
				LoggerUtil.printExtentLog("Pass","Navigated to "+ DriverManager.WEB_DRIVER_THREAD.get().getCurrentUrl()+" URL by clicking Instagram button");
				WebInteractUtil.Waittilljquesryupdated();
				driver.navigate().back();
				WebInteractUtil.Waittilljquesryupdated();
				WebInteractUtil.Click(InfinityLearnPage.InfinityLogo);
				if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.SigninBtn, 60)) {
					LoggerUtil.printExtentLog("Pass", "Scholarship Test page verified");
					Status = "True";
				}else {
					LoggerUtil.printExtentLogWithScreenshot("Fail", "Scholarship Test page verfication failed, Please Check");
                    Status = "False";}
				return Status;
		 }
		 public String NavigateNeet() throws Exception {

			 String Status = null;
			 WebInteractUtil.Click(InfinityLearnPage.NEETButton);
	            WebInteractUtil.Click(InfinityLearnPage.NEETPageVerify);
	            WebInteractUtil.Waittilljquesryupdated();
	           if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.NEETPageVerify, 60))
	           {
	                LoggerUtil.printExtentLog("Pass", "Navigate to NEETPageVerify page is successfull");
	                Status = "True";       
	           }  else   {
	              LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigate to NEET page is not successfull, Please Check");
	                    Status = "False";
	            } 
	            return Status;
	            }
		 public String NavigateJeeAdvanced(Map<String, String> testdata) throws Exception {

			 String Status = null;
				WebInteractUtil.Click(InfinityLearnPage.JeeAdvButton);
				WebInteractUtil.Waittilljquesryupdated();
			   if(WebInteractUtil.waitForElementToBeVisible(InfinityLearnPage.JEEAdvPageVerify, 60)) 
			   {
	                LoggerUtil.printExtentLog("Pass", "Navigate to JEE Advanced page is successfull");
	                Status = "True";        
			   }  else	{
	              LoggerUtil.printExtentLogWithScreenshot("Fail", "Navigate to JEE Advanced page is not successfull, Please Check");
	                    Status = "False";
	              }
	            return Status;
	            }
		 }